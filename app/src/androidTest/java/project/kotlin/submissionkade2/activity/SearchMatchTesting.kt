package project.kotlin.submissionkade2.activity

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import project.kotlin.submissionkade2.R
import project.kotlin.submissionkade2.service.EspressoIdlingResource

/*
Skenario Pengujian

- Pada saat Aplikasi terbuka akan Memuat daftar liga (Idling)
- Lalu setelah selesai akan menampilkan recycler_main
- Scroll Recycler View Main ke Posisi 0
- Memberi Action Click pada item posisi 0 recycler_main
- Memberi Action Click pada FAB speedDial
- Memberi Action Click pada FAB action_search
- Memberi Action Click pada Menu search
- Clear text, type Text "Racing Club" kemudia mengklik actionImeButton
- Memuat daftar Pertandingan (Idling)
- Lalu setelah selesai akan menampilkan recycler_view_search yang berisi pertandingan
 */

@RunWith(AndroidJUnit4::class)
class SearchMatchTesting {
    @Rule
    @JvmField var activityRule = ActivityTestRule(MainActivity::class.java)

    @Before
    fun setUp() {
        EspressoIdlingResource
        IdlingRegistry.getInstance().register(EspressoIdlingResource.idlingresource)
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.idlingresource)
    }

    @Test
    fun RecyclerViewBehaviour() {
        onView(withId(R.id.recycler_main))
            .check(matches(isDisplayed()))
        onView(withId(R.id.recycler_main)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))
        onView(withId(R.id.recycler_main)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        onView(withId(R.id.speedDial))
            .check(matches(isDisplayed()))
        onView(withId(R.id.speedDial)).perform(click())

        onView(withId(R.id.action_search))
            .check(matches(isDisplayed()))
        onView(withId(R.id.action_search)).perform(click())

        onView(withId(R.id.search))
            .check(matches(isDisplayed()))
        onView(withId(R.id.search)).perform(click())

        onView(withId(R.id.search_src_text)).perform(ViewActions.clearText())
        onView(withId(R.id.search_src_text)).perform(ViewActions.typeText("Racing Club"))
        onView(withId(R.id.search_src_text)).perform(ViewActions.pressImeActionButton())

    }
}