package project.kotlin.submissionkade2.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_favorite.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import project.kotlin.submissionkade2.Helper.databaseMatch
import project.kotlin.submissionkade2.R
import project.kotlin.submissionkade2.adapter.RecyclerMatchAdapter
import project.kotlin.submissionkade2.models.FavoriteMatch
import project.kotlin.submissionkade2.models.Match
import project.kotlin.submissionkade2.models.Teams
import project.kotlin.submissionkade2.utils.gone
import project.kotlin.submissionkade2.utils.visible

/**
 * A simple [Fragment] subclass.
 */
class FavoriteFragment : Fragment() {

    private lateinit var adapter: RecyclerMatchAdapter
    private var idLeague: Int? = null
    private var type: Int? = null
    private var match: MutableList<Match> = mutableListOf()
    private var teams: MutableMap<Int?, Teams> = mutableMapOf()

    companion object {
        fun newFragment(idLeague: Int?, type: Int?): FavoriteFragment {
            val fragment = FavoriteFragment()
            val bundle = Bundle()
            if (idLeague != null) {
                bundle.putInt("dataId", idLeague)
                type?.let { bundle.putInt("dataType", it) }
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        idLeague = arguments?.getInt("dataId")
        type = arguments?.getInt("dataType")

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        adapter = RecyclerMatchAdapter(match, context, teams)
        recycler_view_fav.adapter = adapter
        recycler_view_fav.layoutManager = layoutManager
        showFavoriteMatch()
    }

    private fun showFavoriteMatch(){
        context?.databaseMatch?.use {
            val result = select(FavoriteMatch.TABLE_FAVORITE_MATCH).whereArgs("(${FavoriteMatch.LEAGUE_ID} = {leagueId})", "leagueId" to idLeague!!)
            val favorite = result.parseList(classParser<FavoriteMatch>())
            var index = 0
            match.clear()
            val iterator = favorite.listIterator()
            for (item in iterator){
                val matchItem = Match(
                    item.idEvent,
                    item.strHomeTeam,
                    item.strAwayTeam,
                    item.intHomeScore,
                    item.intAwayScore,
                    item.idHomeTeam,
                    item.idAwayTeam,
                    item.dateEvent,
                    item.strAwayBadge,
                    item.strHomeBadge,
                    item.dateEvent,
                    item.idLeague,
                    item.intRound,
                    item.strAwayFormation,
                    item.strAwayGoalDetails,
                    item.strAwayLineupDefense,
                    item.strAwayLineupForward,
                    item.strAwayLineupGoalkeeper,
                    item.strAwayLineupMidfield,
                    item.strAwayLineupSubstitutes,
                    item.strAwayRedCards,
                    item.strAwayYellowCards,
                    item.strEvent,
                    item.strHomeFormation,
                    item.strHomeGoalDetails,
                    item.strHomeLineupDefense,
                    item.strHomeLineupForward,
                    item.strHomeLineupGoalkeeper,
                    item.strHomeLineupMidfield,
                    item.strHomeLineupSubstitutes,
                    item.strHomeRedCards,
                    item.strHomeYellowCards,
                    item.strLeague,
                    item.strSeason,
                    item.strSport,
                    item.strTime,
                    item.strVideo
                )
                if (type?.equals(1)!!){
                    if(matchItem.intHomeScore == null && matchItem.intAwayScore == null){
                        match.add(index, matchItem)
                        index++
                    }
                }
                else{
                    if (matchItem.intHomeScore != null && matchItem.intAwayScore != null){
                        match.add(index, matchItem)
                        index++
                    }
                }

            }

            adapter.notifyDataSetChanged()
            if (match.size > 0){
                tvEmptyFav.gone()
                recycler_view_fav.visible()
            }
            else{
                tvEmptyFav.visible()
            }
        }
    }
}
