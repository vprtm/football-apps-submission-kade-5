package project.kotlin.submissionkade2.Fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_match.*
import project.kotlin.submissionkade2.R
import project.kotlin.submissionkade2.adapter.MatchPagerAdapter
/**
 * A simple [Fragment] subclass.
 */
class MatchFragment : Fragment() {

    private var idLeague: Int? = null
    private var strLeague: String? = null

    companion object {
        fun newFragment(idLeague: Int?, strLeague: String?): MatchFragment {
            val fragment = MatchFragment()
            val bundle = Bundle()
            if (idLeague != null) {
                bundle.putInt("dataId", idLeague)
                bundle.putString("dataName",strLeague)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_match, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        idLeague = arguments?.getInt("dataId")
        strLeague = arguments?.getString("dataName")

        pagerMatch.adapter = idLeague?.let {
            activity?.supportFragmentManager?.let { it1 ->
                MatchPagerAdapter(
                    it1,
                    it, strLeague.toString())
            }
        }
    }
}
