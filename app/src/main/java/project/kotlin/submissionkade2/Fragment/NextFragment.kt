package project.kotlin.submissionkade2.Fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_next.*
import org.jetbrains.anko.support.v4.toast
import project.kotlin.submissionkade2.Presenter.EventPresenter
import project.kotlin.submissionkade2.R
import project.kotlin.submissionkade2.Repository.EventRepository
import project.kotlin.submissionkade2.adapter.RecyclerMatchAdapter
import project.kotlin.submissionkade2.models.*
import project.kotlin.submissionkade2.service.DataRepository
import project.kotlin.submissionkade2.service.EspressoIdlingResource
import project.kotlin.submissionkade2.utils.gone
import project.kotlin.submissionkade2.utils.invisible
import project.kotlin.submissionkade2.utils.visible
import project.kotlin.submissionkade2.view.EventView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NextFragment : Fragment(), EventView {

    private var itemEvent: MutableList<EventItem> = mutableListOf()
    private var match: MutableList<Match> = mutableListOf(Match(null, null, null, null, null, null, null, null, null, null, null, null, null,null, null, null,null, null, null,null, null, null,null, null, null,null, null, null,null, null, null,null, null, null,null, null,null))
    private var teams: MutableMap<Int?, Teams> = mutableMapOf()
    private lateinit var adapter: RecyclerMatchAdapter
    private lateinit var eventPresenter :EventPresenter
    private var idLeague: Int? = null
    private var strLeague: String? = null

    companion object {
        fun newFragment(idLeague: Int?, strLeague: String?): NextFragment {
            val fragment = NextFragment()
            val bundle = Bundle()
            if (idLeague != null) {
                bundle.putInt("dataId", idLeague)
            }
            bundle.putString("dataName", strLeague)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_next, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        idLeague = arguments?.getInt("dataId")
        strLeague = arguments?.getString("dataName")

        eventPresenter = EventPresenter(this, EventRepository())

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        adapter = RecyclerMatchAdapter(match, context, teams)
        recycler_view_next.adapter = adapter
        recycler_view_next.layoutManager = layoutManager

        EspressoIdlingResource.increment()
        idLeague?.let { eventPresenter.getNext(it) }
    }

    fun showEmpty() {
        progressBarNext.gone()
        recycler_view_next.gone()
        tvEmptyNext.visible()
    }

    fun showMatchList()
    {
        if (!EspressoIdlingResource.idlingresource.isIdleNow) {
            EspressoIdlingResource.decrement()
        }
        match.removeAt(match.lastIndex)
        adapter.notifyDataSetChanged()
        recycler_view_next.visible()
        progressBarNext.gone()
    }

    fun addTeam(teamList: List<Teams>) {
        val iterator = teamList.listIterator()
        for (item in iterator) {
            teams.put(item.idTeam, item)
        }
        addMatchList()
    }

    fun addMatchList() {
        val iterator = itemEvent.listIterator()
        var index = 0
        for (item in iterator) {
            val matchItem = Match(
                item.idEvent,
                item.strHomeTeam,
                item.strAwayTeam,
                item.intHomeScore,
                item.intAwayScore,
                item.idHomeTeam,
                item.idAwayTeam,
                item.dateEvent,
                teams.get(item.idAwayTeam)?.strTeamBadge,
                teams.get(item.idHomeTeam)?.strTeamBadge,
                item.dateEvent,
                item.idLeague,
                item.intRound,
                item.strAwayFormation,
                item.strAwayGoalDetails,
                item.strAwayLineupDefense,
                item.strAwayLineupForward,
                item.strAwayLineupGoalkeeper,
                item.strAwayLineupMidfield,
                item.strAwayLineupSubstitutes,
                item.strAwayRedCards,
                item.strAwayYellowCards,
                item.strEvent,
                item.strHomeFormation,
                item.strHomeGoalDetails,
                item.strHomeLineupDefense,
                item.strHomeLineupForward,
                item.strHomeLineupGoalkeeper,
                item.strHomeLineupMidfield,
                item.strHomeLineupSubstitutes,
                item.strHomeRedCards,
                item.strHomeYellowCards,
                item.strLeague,
                item.strSeason,
                item.strSport,
                item.strTime,
                item.strVideo
            )
            match.add(index, matchItem)
            index++
        }
        showMatchList()
    }

    fun getTeamList() {
        val teamServices = DataRepository.createTeams()
        teamServices.getTeam(strLeague).enqueue(object : Callback<TeamsResponse> {
            override fun onResponse(call: Call<TeamsResponse>, response: Response<TeamsResponse>) {
                if (response.isSuccessful) {
                    response.body()?.listTeam?.let { addTeam(it) }
                }
            }

            override fun onFailure(call: Call<TeamsResponse>, error: Throwable) {
                Log.e("tag", "errornya ${error.message}")
            }

        })
    }

    override fun onShowLoadingEvent() {
        recycler_view_next.invisible()
        progressBarNext.visible()
    }

    override fun onHideLoadingEvent() {
    }

    override fun onDataLoadedEvent(data: EventResponse?) {
        if(data?.listEvent != null){
            itemEvent = data?.listEvent as MutableList<EventItem>
            getTeamList()
        }
        else showEmpty()
    }

    override fun onDataErrorEvent() {
        toast("Failed Loading Match Data")
    }
}
