package project.kotlin.submissionkade2.Fragment


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_standing.*
import org.jetbrains.anko.support.v4.toast
import project.kotlin.submissionkade2.Presenter.StandingPresenter
import project.kotlin.submissionkade2.R
import project.kotlin.submissionkade2.Repository.LeagueRepository
import project.kotlin.submissionkade2.adapter.RecycleStandingAdapter
import project.kotlin.submissionkade2.models.*
import project.kotlin.submissionkade2.service.DataRepository
import project.kotlin.submissionkade2.utils.gone
import project.kotlin.submissionkade2.utils.invisible
import project.kotlin.submissionkade2.utils.visible
import project.kotlin.submissionkade2.view.StandingView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class StandingFragment : Fragment(), StandingView {

    private var itemStanding: MutableList<StandingItem> = mutableListOf()
    private var klasemen: MutableList<Klasemen> = mutableListOf()
    private lateinit var adapter: RecycleStandingAdapter
    private lateinit var standingPresenter : StandingPresenter
    private var idLeague: Int? = null
    private var strLeague: String? = null
    private var teams: MutableMap<Int?, Teams> = mutableMapOf()

    companion object {
        fun newFragment(idLeague: Int?, strLeague : String?): StandingFragment {
            val fragment = StandingFragment()
            val bundle = Bundle()
            if (idLeague != null) {
                bundle.putInt("dataId", idLeague)
                bundle.putString("dataName", strLeague)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_standing, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        idLeague = arguments?.getInt("dataId")
        strLeague = arguments?.getString("dataName")
        standingPresenter = StandingPresenter(this, LeagueRepository())

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        adapter = RecycleStandingAdapter(klasemen, context)
        recycler_view_standing.adapter = adapter
        recycler_view_standing.layoutManager = layoutManager

        idLeague?.let { standingPresenter.getStanding(it) }
    }

    fun showEmpty() {
        progressBarStanding.gone()
        recycler_view_standing.gone()
        tvEmptyStanding.visible()
    }

    fun showKlasemenList()
    {
        adapter.notifyDataSetChanged()
        recycler_view_standing.visible()
        standingTitle.visible()
        borderStanding.visible()
        progressBarStanding.gone()
    }

    fun addKlasemenList() {
        val iterator = itemStanding.listIterator()
        var index = 0
        for (item in iterator) {
            val klasemenItem = Klasemen(
                item.name,
                item.teamid,
                teams.get(item.teamid)?.strTeamBadge,
                item.played,
                item.goalsfor,
                item.goalsagainst,
                item.goalsdifference,
                item.win,
                item.draw,
                item.loss,
                item.total
            )
            klasemen.add(index, klasemenItem)
            index++
        }
        showKlasemenList()
    }

    fun addTeam(teamList: List<Teams>) {
        val iterator = teamList.listIterator()
        for (item in iterator) {
            teams.put(item.idTeam, item)
        }
        addKlasemenList()
    }

    fun getTeamList() {
        val teamServices = DataRepository.createTeams()
        teamServices.getTeam(strLeague).enqueue(object : Callback<TeamsResponse> {
            override fun onResponse(call: Call<TeamsResponse>, response: Response<TeamsResponse>) {
                if (response.isSuccessful) {
                    response.body()?.listTeam?.let { addTeam(it) }
                }
            }

            override fun onFailure(call: Call<TeamsResponse>, error: Throwable) {
                Log.e("tag", "errornya ${error.message}")
            }

        })
    }

    override fun onShowLoading() {
        recycler_view_standing.invisible()
        progressBarStanding.visible()
    }

    override fun onHideLoading() {
    }

    override fun onDataLeagueLoaded(data: StandingResponse?) {
        if(data?.listStanding != null){
            itemStanding = data?.listStanding as MutableList<StandingItem>
            getTeamList()
        }
        else showEmpty()
    }

    override fun onDataLeagueError() {
        toast("Failed Loading Match Data")
    }


}
