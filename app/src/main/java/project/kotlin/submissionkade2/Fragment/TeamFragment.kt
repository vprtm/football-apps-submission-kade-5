package project.kotlin.submissionkade2.Fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_team.*
import org.jetbrains.anko.support.v4.toast
import project.kotlin.submissionkade2.Presenter.TeamsPresenter
import project.kotlin.submissionkade2.R
import project.kotlin.submissionkade2.Repository.TeamsRepository
import project.kotlin.submissionkade2.adapter.RecycleTeamsAdapter
import project.kotlin.submissionkade2.models.Teams
import project.kotlin.submissionkade2.models.TeamsResponse
import project.kotlin.submissionkade2.utils.gone
import project.kotlin.submissionkade2.utils.invisible
import project.kotlin.submissionkade2.utils.visible
import project.kotlin.submissionkade2.view.TeamsView

/**
 * A simple [Fragment] subclass.
 */
class TeamFragment : Fragment(),TeamsView {

    private var itemTeams: MutableList<Teams> = mutableListOf()
    private var idLeague: Int? = null
    private var strLeague: String? = null
    private lateinit var adapter: RecycleTeamsAdapter
    private lateinit var teamsPresenter : TeamsPresenter

    companion object {
        fun newFragment(idLeague: Int?, strLeague : String?): TeamFragment {
            val fragment = TeamFragment()
            val bundle = Bundle()
            if (idLeague != null) {
                bundle.putInt("dataId", idLeague)
                bundle.putString("dataName", strLeague)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_team, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        idLeague = arguments?.getInt("dataId")
        strLeague = arguments?.getString("dataName")
        teamsPresenter = TeamsPresenter(this, TeamsRepository())

        val a = Teams(1,1,1,1,1,"a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a")
        itemTeams.add(a)
        itemTeams.add(a)

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        adapter = RecycleTeamsAdapter(itemTeams, context)
        recycler_view_team.adapter = adapter
        recycler_view_team.layoutManager = layoutManager

        strLeague?.let { teamsPresenter.getAllTeams(it) }
    }

    fun showEmpty() {
        progressBarTeam.gone()
        recycler_view_team.invisible()
        tvEmptyTeam.visible()
    }

    fun showTeamList()
    {
        tvTextTeams.visible()
        dividerTextTeams.visible()
        adapter.notifyDataSetChanged()
        progressBarTeam.gone()
        recycler_view_team.visible()
    }

    override fun onShowLoading() {
        tvTextTeams.gone()
        dividerTextTeams.gone()
        progressBarTeam.visible()
        recycler_view_team.invisible()
    }

    override fun onHideLoading() {
    }

    override fun onDataTeamsLoaded(data: TeamsResponse?) {
        if(data?.listTeam != null){
            itemTeams.clear()
            itemTeams.addAll(data.listTeam)
            showTeamList()
        }
        else showEmpty()
    }

    override fun onDataTeamsError() {
        toast("Failed Loading Teams Data")
    }
}
