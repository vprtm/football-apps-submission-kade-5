package project.kotlin.submissionkade2.Helper

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*
import project.kotlin.submissionkade2.models.FavoriteMatch

class FavoriteMatchHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "match.db", null, 1) {
    companion object {
        private var instance: FavoriteMatchHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): FavoriteMatchHelper {
            if (instance == null) {
                instance = FavoriteMatchHelper(ctx.applicationContext)
            }
            return instance as FavoriteMatchHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        // Here you create tables
        db.createTable(
            FavoriteMatch.TABLE_FAVORITE_MATCH, true,
            FavoriteMatch.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            FavoriteMatch.MATCH_ID to INTEGER + UNIQUE,
            FavoriteMatch.HOME_NAME to TEXT,
            FavoriteMatch.AWAY_NAME to TEXT,
            FavoriteMatch.HOME_SCORE to INTEGER,
            FavoriteMatch.AWAY_SCORE to INTEGER,
            FavoriteMatch.HOME_ID to INTEGER,
            FavoriteMatch.AWAY_ID to INTEGER,
            FavoriteMatch.STR_DATE to TEXT,
            FavoriteMatch.HOME_BADGE to TEXT,
            FavoriteMatch.AWAY_BADGE to TEXT,
            FavoriteMatch.MATCH_DATE to TEXT,
            FavoriteMatch.LEAGUE_ID to INTEGER,
            FavoriteMatch.INT_ROUND to INTEGER,
            FavoriteMatch.AWAY_FORM to TEXT,
            FavoriteMatch.AWAY_GOAL to TEXT,
            FavoriteMatch.AWAY_DEFENSE to TEXT,
            FavoriteMatch.AWAY_FORWARD to TEXT,
            FavoriteMatch.AWAY_GK to TEXT,
            FavoriteMatch.AWAY_MID to TEXT,
            FavoriteMatch.AWAY_SUB to TEXT,
            FavoriteMatch.AWAY_RED to TEXT,
            FavoriteMatch.AWAY_YELLOW to TEXT,
            FavoriteMatch.MATCH_NAME to TEXT,
            FavoriteMatch.HOME_FORM to TEXT,
            FavoriteMatch.HOME_GOAL to TEXT,
            FavoriteMatch.HOME_DEFENSE to TEXT,
            FavoriteMatch.HOME_FORWARD to TEXT,
            FavoriteMatch.HOME_GK to TEXT,
            FavoriteMatch.HOME_MID to TEXT,
            FavoriteMatch.HOME_SUB to TEXT,
            FavoriteMatch.HOME_RED to TEXT,
            FavoriteMatch.HOME_YELLOW to TEXT,
            FavoriteMatch.LEAGUE_NAME to TEXT,
            FavoriteMatch.SEASON_NAME to TEXT,
            FavoriteMatch.SPORT_NAME to TEXT,
            FavoriteMatch.TIME_STR to TEXT,
            FavoriteMatch.VIDEO_STR to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // Here you can upgrade tables, as usual
        db.dropTable(FavoriteMatch.TABLE_FAVORITE_MATCH, true)
    }
}

// Access property for Context
val Context.databaseMatch: FavoriteMatchHelper
    get() = FavoriteMatchHelper.getInstance(this)