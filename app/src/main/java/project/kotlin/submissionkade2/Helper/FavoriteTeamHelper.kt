package project.kotlin.submissionkade2.Helper

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*
import project.kotlin.submissionkade2.models.FavoriteTeam

class FavoriteTeamHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "team.db", null, 2) {
    companion object {
        private var instance: FavoriteTeamHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): FavoriteTeamHelper {
            if (instance == null) {
                instance = FavoriteTeamHelper(ctx.applicationContext)
            }
            return instance as FavoriteTeamHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        // Here you create tables
        db.createTable(
            FavoriteTeam.TABLE_FAVORITE_TEAM, true,
            FavoriteTeam.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            FavoriteTeam.TEAM_ID to INTEGER + UNIQUE,
            FavoriteTeam.LEAGUE_ID to INTEGER,
            FavoriteTeam.STR_TEAM to TEXT,
            FavoriteTeam.STR_LEAGUE to TEXT,
            FavoriteTeam.STR_DESC_EN to TEXT,
            FavoriteTeam.STR_TEAM_BADGE to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // Here you can upgrade tables, as usual
        db.dropTable(FavoriteTeam.TABLE_FAVORITE_TEAM, true)
    }
}

// Access property for Context
val Context.databaseTeam: FavoriteTeamHelper
    get() = FavoriteTeamHelper.getInstance(this)