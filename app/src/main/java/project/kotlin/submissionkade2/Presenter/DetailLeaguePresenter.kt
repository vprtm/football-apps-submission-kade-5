package project.kotlin.submissionkade2.Presenter

import project.kotlin.submissionkade2.Repository.LeagueRepository
import project.kotlin.submissionkade2.Repository.LeagueRepositoryCallback
import project.kotlin.submissionkade2.models.DetailLeagueResponse
import project.kotlin.submissionkade2.view.DetailLeagueView

class DetailLeaguePresenter (private val view: DetailLeagueView, private val leagueRepository: LeagueRepository){

    fun getLeague(id:Int) {
        view.onShowLoadingDetailLeague()
        leagueRepository.getDetailLeague(id,object : LeagueRepositoryCallback<DetailLeagueResponse?> {
            override fun onDataLeagueLoaded(data: DetailLeagueResponse?) {
                view.onDataLeagueLoaded(data)
            }

            override fun onDataLeagueError() {
                view.onDataLeagueError()
            }
        })
        view.onHideLoadingDetailLeague()
    }
}