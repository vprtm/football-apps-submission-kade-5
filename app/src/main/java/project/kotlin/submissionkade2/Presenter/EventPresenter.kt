package project.kotlin.submissionkade2.Presenter

import project.kotlin.submissionkade2.Repository.EventRepository
import project.kotlin.submissionkade2.Repository.EventRepositoryCallback
import project.kotlin.submissionkade2.models.EventResponse
import project.kotlin.submissionkade2.view.EventView

class EventPresenter(private val view: EventView, private val eventRepository: EventRepository){

    fun getNext(id: Int) {
        view.onShowLoadingEvent()
        eventRepository.getNextMatch(id, object : EventRepositoryCallback<EventResponse?> {
            override fun onDataLoadedEvent(data: EventResponse?) {
                view.onDataLoadedEvent(data)
            }

            override fun onDataErrorEvent() {
                view.onDataErrorEvent()
            }
        })
        view.onHideLoadingEvent()
    }

    fun getPrevious(id: Int) {
        view.onShowLoadingEvent()
        eventRepository.getPreviousMatch(id, object : EventRepositoryCallback<EventResponse?> {
            override fun onDataLoadedEvent(data: EventResponse?) {
                view.onDataLoadedEvent(data)
            }

            override fun onDataErrorEvent() {
                view.onDataErrorEvent()
            }
        })
        view.onHideLoadingEvent()
    }

    fun getDetail(id: Int) {
        view.onShowLoadingEvent()
        eventRepository.getDetailMatch(id, object : EventRepositoryCallback<EventResponse?> {
            override fun onDataLoadedEvent(data: EventResponse?) {
                view.onDataLoadedEvent(data)
            }

            override fun onDataErrorEvent() {
                view.onDataErrorEvent()
            }
        })
        view.onHideLoadingEvent()
    }
}