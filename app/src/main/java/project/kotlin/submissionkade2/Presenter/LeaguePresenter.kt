package project.kotlin.submissionkade2.Presenter

import project.kotlin.submissionkade2.Repository.LeagueRepository
import project.kotlin.submissionkade2.Repository.LeagueRepositoryCallback
import project.kotlin.submissionkade2.models.LeagueResponse
import project.kotlin.submissionkade2.view.LeagueView


class LeaguePresenter (private val view: LeagueView, private val leagueRepository: LeagueRepository){

    fun getLeague() {
        view.onShowLoadingLeague()
        leagueRepository.getLeague(object : LeagueRepositoryCallback<LeagueResponse?> {
            override fun onDataLeagueLoaded(data: LeagueResponse?) {
                view.onDataLeagueLoaded(data)
            }

            override fun onDataLeagueError() {
                view.onDataLeagueError()
            }
        })
        view.onHideLoadingLeague()
    }
}