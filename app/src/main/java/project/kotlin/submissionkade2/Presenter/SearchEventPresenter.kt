package project.kotlin.submissionkade2.Presenter

import project.kotlin.submissionkade2.Repository.EventRepository
import project.kotlin.submissionkade2.Repository.EventRepositoryCallback
import project.kotlin.submissionkade2.models.SearchResponse
import project.kotlin.submissionkade2.view.SearchEventView

class SearchEventPresenter(private val view: SearchEventView, private val eventRepository: EventRepository){

    fun getSearch(query: String) {
        view.onShowLoadingSearch()
        eventRepository.searchMatch(query, object : EventRepositoryCallback<SearchResponse?> {
            override fun onDataLoadedEvent(data: SearchResponse?) {
                view.onDataLoadedEvent(data)
            }

            override fun onDataErrorEvent() {
                view.onDataErrorEvent()
            }
        })
        view.onHideLoadingSearch()
    }
}