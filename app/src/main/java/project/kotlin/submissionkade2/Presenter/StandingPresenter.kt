package project.kotlin.submissionkade2.Presenter

import project.kotlin.submissionkade2.Repository.LeagueRepository
import project.kotlin.submissionkade2.Repository.LeagueRepositoryCallback
import project.kotlin.submissionkade2.models.StandingResponse
import project.kotlin.submissionkade2.view.StandingView

class StandingPresenter(private val view: StandingView, private val leagueRepository: LeagueRepository){

    fun getStanding(id: Int) {
        view.onShowLoading()
        leagueRepository.getStanding(id, object : LeagueRepositoryCallback<StandingResponse?> {
            override fun onDataLeagueLoaded(data: StandingResponse?) {
                view.onDataLeagueLoaded(data)
            }

            override fun onDataLeagueError() {
                view.onDataLeagueError()
            }
        })
        view.onHideLoading()
    }
}