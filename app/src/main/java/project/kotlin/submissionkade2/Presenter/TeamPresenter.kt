package project.kotlin.submissionkade2.Presenter

import project.kotlin.submissionkade2.Repository.TeamsRepository
import project.kotlin.submissionkade2.Repository.TeamsRepositoryCallback
import project.kotlin.submissionkade2.models.TeamResponse
import project.kotlin.submissionkade2.view.TeamView

class TeamPresenter(private val view: TeamView, private val teamsRepository: TeamsRepository){

    fun getTeamDetail(idTeam: Int) {
        view.onShowLoading()
        teamsRepository.getTeamDetail(idTeam, object : TeamsRepositoryCallback<TeamResponse?> {
            override fun onDataTeamsLoaded(data: TeamResponse?) {
                view.onDataTeamsLoaded(data)
            }

            override fun onDataTeamsError() {
                view.onDataTeamsError()
            }
        })
        view.onHideLoading()
    }

    fun searchTeam(query: String) {
        view.onShowLoading()
        teamsRepository.searchTeam(query, object : TeamsRepositoryCallback<TeamResponse?> {
            override fun onDataTeamsLoaded(data: TeamResponse?) {
                view.onDataTeamsLoaded(data)
            }

            override fun onDataTeamsError() {
                view.onDataTeamsError()
            }
        })
        view.onHideLoading()
    }
}