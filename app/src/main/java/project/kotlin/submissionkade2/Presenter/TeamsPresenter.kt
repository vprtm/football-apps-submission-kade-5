package project.kotlin.submissionkade2.Presenter

import project.kotlin.submissionkade2.Repository.TeamsRepository
import project.kotlin.submissionkade2.Repository.TeamsRepositoryCallback
import project.kotlin.submissionkade2.models.TeamsResponse
import project.kotlin.submissionkade2.view.TeamsView

class TeamsPresenter(private val view: TeamsView, private val teamsRepository: TeamsRepository){

    fun getAllTeams(strLeague: String) {
        view.onShowLoading()
        teamsRepository.getAllTeams(strLeague, object : TeamsRepositoryCallback<TeamsResponse?> {
            override fun onDataTeamsLoaded(data: TeamsResponse?) {
                view.onDataTeamsLoaded(data)
            }

            override fun onDataTeamsError() {
                view.onDataTeamsError()
            }
        })
        view.onHideLoading()
    }
}