package project.kotlin.submissionkade2.Repository

import project.kotlin.submissionkade2.models.EventResponse
import project.kotlin.submissionkade2.models.SearchResponse
import project.kotlin.submissionkade2.service.DataRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EventRepository {

    fun getNextMatch(id: Int, callback: EventRepositoryCallback<EventResponse?>) {
        val matchServices = DataRepository.createEvent()
        matchServices.getNext(id).enqueue(object : Callback<EventResponse> {
            override fun onResponse(call: Call<EventResponse>, response: Response<EventResponse>) {
                response?.let {
                    if (it.isSuccessful) {
                        callback.onDataLoadedEvent(it.body())
                    } else {
                        callback.onDataErrorEvent()
                    }
                }
            }

            override fun onFailure(call: Call<EventResponse>, error: Throwable) {
                callback.onDataErrorEvent()
            }

        })
    }

    fun getPreviousMatch(id: Int, callback: EventRepositoryCallback<EventResponse?>) {
        val matchServices = DataRepository.createEvent()
        matchServices.getPrevious(id).enqueue(object : Callback<EventResponse> {
            override fun onResponse(call: Call<EventResponse>, response: Response<EventResponse>) {
                response?.let {
                    if (it.isSuccessful) {
                        callback.onDataLoadedEvent(it.body())
                    } else {
                        callback.onDataErrorEvent()
                    }
                }
            }

            override fun onFailure(call: Call<EventResponse>, error: Throwable) {
                callback.onDataErrorEvent()
            }

        })
    }

    fun getDetailMatch(id: Int, callback: EventRepositoryCallback<EventResponse?>) {
        val matchServices = DataRepository.createEvent()
        matchServices.detailEvent(id).enqueue(object : Callback<EventResponse> {
            override fun onResponse(call: Call<EventResponse>, response: Response<EventResponse>) {
                response?.let {
                    if (it.isSuccessful) {
                        callback.onDataLoadedEvent(it.body())
                    } else {
                        callback.onDataErrorEvent()
                    }
                }
            }

            override fun onFailure(call: Call<EventResponse>, error: Throwable) {
                callback.onDataErrorEvent()
            }

        })
    }

    fun searchMatch(query: String, callback: EventRepositoryCallback<SearchResponse?>) {
        val matchServices = DataRepository.createEvent()
        matchServices.searchEvent(query).enqueue(object : Callback<SearchResponse> {
            override fun onResponse(call: Call<SearchResponse>, response: Response<SearchResponse>) {
                response?.let {
                    if (it.isSuccessful) {
                        callback.onDataLoadedEvent(it.body())
                    } else {
                        callback.onDataErrorEvent()
                    }
                }
            }

            override fun onFailure(call: Call<SearchResponse>, error: Throwable) {
                callback.onDataErrorEvent()
            }

        })
    }

}