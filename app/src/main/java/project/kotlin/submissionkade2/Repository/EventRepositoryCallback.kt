package project.kotlin.submissionkade2.Repository

interface EventRepositoryCallback<T>{
    fun onDataLoadedEvent(data: T?)
    fun onDataErrorEvent()
}