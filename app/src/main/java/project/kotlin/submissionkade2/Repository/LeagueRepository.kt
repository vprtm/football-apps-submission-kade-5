package project.kotlin.submissionkade2.Repository

import project.kotlin.submissionkade2.models.DetailLeagueResponse
import project.kotlin.submissionkade2.models.EventResponse
import project.kotlin.submissionkade2.models.LeagueResponse
import project.kotlin.submissionkade2.models.StandingResponse
import project.kotlin.submissionkade2.service.DataRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LeagueRepository{

    fun getLeague(callback: LeagueRepositoryCallback<LeagueResponse?>) {
        val leagueServices = DataRepository.createLeague()
        leagueServices.getPosts().enqueue(object : Callback<LeagueResponse> {
            override fun onResponse(call: Call<LeagueResponse>, response: Response<LeagueResponse>) {
                response?.let {
                    if (it.isSuccessful) {
                        callback.onDataLeagueLoaded(it.body())
                    } else {
                        callback.onDataLeagueError()
                    }
                }
            }

            override fun onFailure(call: Call<LeagueResponse>, error: Throwable) {
                callback.onDataLeagueError()
            }

        })
    }

    fun getDetailLeague(id : Int, callback: LeagueRepositoryCallback<DetailLeagueResponse?>) {
        val leagueServices = DataRepository.createLeague()
        leagueServices.searchLeague(id).enqueue(object : Callback<DetailLeagueResponse> {
            override fun onResponse(call: Call<DetailLeagueResponse>, response: Response<DetailLeagueResponse>) {
                response?.let {
                    if (it.isSuccessful) {
                        callback.onDataLeagueLoaded(it.body())
                    } else {
                        callback.onDataLeagueError()
                    }
                }
            }

            override fun onFailure(call: Call<DetailLeagueResponse>, error: Throwable) {
                callback.onDataLeagueError()
            }

        })
    }

    fun getStanding(id : Int, callback: LeagueRepositoryCallback<StandingResponse?>) {
        val leagueServices = DataRepository.createLeague()
        leagueServices.getStanding(id).enqueue(object : Callback<StandingResponse> {
            override fun onResponse(call: Call<StandingResponse>, response: Response<StandingResponse>) {
                response?.let {
                    if (it.isSuccessful) {
                        callback.onDataLeagueLoaded(it.body())
                    } else {
                        callback.onDataLeagueError()
                    }
                }
            }

            override fun onFailure(call: Call<StandingResponse>, error: Throwable) {
                callback.onDataLeagueError()
            }

        })
    }
}