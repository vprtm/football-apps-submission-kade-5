package project.kotlin.submissionkade2.Repository

interface LeagueRepositoryCallback<T>{
    fun onDataLeagueLoaded(data: T?)
    fun onDataLeagueError()
}