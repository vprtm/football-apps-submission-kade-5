package project.kotlin.submissionkade2.Repository

import android.util.Log
import project.kotlin.submissionkade2.models.TeamResponse
import project.kotlin.submissionkade2.models.TeamsResponse
import project.kotlin.submissionkade2.service.DataRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TeamsRepository{

    fun getAllTeams(strLeague : String, callback: TeamsRepositoryCallback<TeamsResponse?>){
        val teamServices = DataRepository.createTeams()
        teamServices.getTeam(strLeague).enqueue(object : Callback<TeamsResponse> {
            override fun onResponse(call: Call<TeamsResponse>, response: Response<TeamsResponse>) {
                if (response.isSuccessful) {
                    callback.onDataTeamsLoaded(response.body())
                }
                else{
                    callback.onDataTeamsError()
                }
            }

            override fun onFailure(call: Call<TeamsResponse>, error: Throwable) {
                callback.onDataTeamsError()
            }

        })
    }

    fun getTeamDetail(idTeam : Int, callback: TeamsRepositoryCallback<TeamResponse?>){
        val teamServices = DataRepository.createTeams()
        teamServices.getTeamDetail(idTeam).enqueue(object : Callback<TeamResponse> {
            override fun onResponse(call: Call<TeamResponse>, response: Response<TeamResponse>) {
                if (response.isSuccessful) {
                    callback.onDataTeamsLoaded(response.body())
                }
                else{
                    callback.onDataTeamsError()
                }
            }

            override fun onFailure(call: Call<TeamResponse>, error: Throwable) {
                callback.onDataTeamsError()
            }

        })
    }

    fun searchTeam(query : String, callback: TeamsRepositoryCallback<TeamResponse?>){
        val teamServices = DataRepository.createTeams()
        teamServices.searchTeam(query).enqueue(object : Callback<TeamResponse> {
            override fun onResponse(call: Call<TeamResponse>, response: Response<TeamResponse>) {
                if (response.isSuccessful) {
                    callback.onDataTeamsLoaded(response.body())
                }
                else{
                    callback.onDataTeamsError()
                }
            }

            override fun onFailure(call: Call<TeamResponse>, error: Throwable) {
                callback.onDataTeamsError()
            }

        })
    }
}