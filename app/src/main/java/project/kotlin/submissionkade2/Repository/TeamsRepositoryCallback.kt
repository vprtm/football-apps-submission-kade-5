package project.kotlin.submissionkade2.Repository

interface TeamsRepositoryCallback<T>{
    fun onDataTeamsLoaded(data: T?)
    fun onDataTeamsError()
}