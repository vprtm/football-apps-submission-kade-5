package project.kotlin.submissionkade2.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.squareup.picasso.Picasso
import project.kotlin.submissionkade2.R
import kotlinx.android.synthetic.main.activity_detail_league.*
import org.jetbrains.anko.browse
import org.jetbrains.anko.toast
import project.kotlin.submissionkade2.Presenter.DetailLeaguePresenter
import project.kotlin.submissionkade2.Repository.LeagueRepository
import project.kotlin.submissionkade2.models.DetailLeagueResponse
import project.kotlin.submissionkade2.models.League
import project.kotlin.submissionkade2.utils.gone
import project.kotlin.submissionkade2.utils.invisible
import project.kotlin.submissionkade2.utils.visible
import project.kotlin.submissionkade2.view.DetailLeagueView

class DetailLeagueActivity : AppCompatActivity(),DetailLeagueView{

    var league: League? = null
    var idLeague : Int? = null
    private lateinit var leaguePresenter : DetailLeaguePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_league)

        idLeague = intent?.extras?.getInt("dataLeague")
        leaguePresenter = DetailLeaguePresenter(this, LeagueRepository())
        setSupportActionBar(toolbarDetailLeague)
        idLeague?.let { leaguePresenter.getLeague(it) }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    fun addDetail(league : League?) {
        this.league = league
        showDetail()
    }

    fun show(){
        progressBarDetail.gone()
        leagueDetail1.visible()
        leagueDetail2.visible()
        leagueDetail3.visible()
    }

    fun showDetail() {
        show()
        supportActionBar?.title = league?.strLeague
        tvDetailName.text = league?.strLeague
        tvDetaildetail.text = league?.strDescriptionEN
        tvDetailYear.text = league?.intFormedYear.toString()

        if (league?.strBadge != null) Picasso.get().load(league?.strBadge).into(ivDetailLeague)

        if(league?.strWebsite != null)fabWeb.setOnClickListener { browse("https://${league?.strWebsite}",true) }
        else fabWeb.setOnClickListener { toast("Website not Available") }

        if(league?.strYoutube != null)fabYoutube.setOnClickListener { browse("https://${league?.strYoutube}",true) }
        else fabYoutube.setOnClickListener { toast("Youtube not Available") }

        if(league?.strFacebook != null)fabFacebook.setOnClickListener { browse("https://${league?.strFacebook}",true) }
        else fabFacebook.setOnClickListener { toast("Facebook not Available") }
    }

    override fun onShowLoadingDetailLeague() {
        progressBarDetail.visible()
        leagueDetail1.invisible()
        leagueDetail2.invisible()
        leagueDetail3.invisible()
    }

    override fun onHideLoadingDetailLeague() {
    }

    override fun onDataLeagueLoaded(data: DetailLeagueResponse?) {
        addDetail(data?.listLeague?.get(0))
    }

    override fun onDataLeagueError() {
        toast("Failed Loaded League Detail")
    }
}

