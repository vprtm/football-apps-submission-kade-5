package project.kotlin.submissionkade2.activity

import android.app.ProgressDialog
import android.database.sqlite.SQLiteConstraintException
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.squareup.picasso.Picasso
import project.kotlin.submissionkade2.R
import kotlinx.android.synthetic.main.activity_detail_match.*
import org.jetbrains.anko.*
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import project.kotlin.submissionkade2.Helper.databaseMatch
import project.kotlin.submissionkade2.models.*
import project.kotlin.submissionkade2.service.DataRepository
import project.kotlin.submissionkade2.utils.gone
import project.kotlin.submissionkade2.utils.visible
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailMatchActivity : AppCompatActivity() {

    var match: Match? = null
    var isFavorited: Boolean = false
    private lateinit var mProgressDialog :  ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_match)
        match = intent?.getParcelableExtra("match")
        mProgressDialog =  indeterminateProgressDialog("Loading Match Data...")
        mProgressDialog.show()
        getDetailEvent()
    }

    private fun getDetailEvent(){
        val eventService = DataRepository.createEvent()
        eventService.detailEvent(match?.idEvent).enqueue(object : Callback<EventResponse>{
            override fun onResponse(call: Call<EventResponse>, response: Response<EventResponse>) {
                if (response.isSuccessful) {
                    showMatch(response.body()!!.listEvent.get(0))
                }
            }

            override fun onFailure(call: Call<EventResponse>, error: Throwable) {
                Log.e("tag", "errornya ${error.message}")
            }
        })
    }

    private fun showMatch(event : EventItem){
        tvMatchName.text = event.strEvent
        tvMatchDate.text = event.dateEvent
        tvDetailNameA.text = event.strHomeTeam
        tvDetailNameB.text = event.strAwayTeam

        if (match?.intAwayScore != null) {
            tvDetailSkorA.text = event.intHomeScore.toString()
            tvDetailSkorB.text = event.intAwayScore.toString()
        }

        Picasso.get().load(match?.strHomeBadge).into(ivDetailTeamA)
        Picasso.get().load(match?.strAwayBadge).into(ivDetailTeamB)

        if (event.strVideo == null || event.strVideo == "") btnYoutube.gone()
        else{
            btnYoutube.visible()
            btnYoutube.setOnClickListener {
                browse(event.strVideo.toString())
            }
        }

        tvHomeGoal.text = event.strHomeGoalDetails
        tvAwayGoal.text = event.strAwayGoalDetails
        tvHomeSubtitutes.text = event.strHomeLineupSubstitutes
        tvAwaySubtitutes.text = event.strAwayLineupSubstitutes
        tvHomeYellowCard.text = event.strHomeYellowCards
        tvAwayYellowCard.text = event.strAwayYellowCards
        tvHomeRedCard.text = event.strHomeRedCards
        tvAwayRedCard.text = event.strAwayRedCards

        checkFavorite()

        btnBack.setOnClickListener {
            onBackPressed()
        }

        btnLove.setOnClickListener {
            if (isFavorited) removeFromFavorite()
            else addToFavorite()
        }

        mProgressDialog.hide()
    }

    private fun addToFavorite() {
        try {
            databaseMatch.use {
                insert(
                    FavoriteMatch.TABLE_FAVORITE_MATCH,
                    FavoriteMatch.MATCH_ID to match?.idEvent,
                    FavoriteMatch.HOME_NAME to match?.strHomeTeam,
                    FavoriteMatch.AWAY_NAME to match?.strAwayTeam,
                    FavoriteMatch.HOME_SCORE to match?.intHomeScore,
                    FavoriteMatch.AWAY_SCORE to match?.intAwayScore,
                    FavoriteMatch.HOME_ID to match?.idHomeTeam,
                    FavoriteMatch.AWAY_ID to match?.idAwayTeam,
                    FavoriteMatch.STR_DATE to match?.strDate,
                    FavoriteMatch.HOME_BADGE to match?.strHomeBadge,
                    FavoriteMatch.AWAY_BADGE to match?.strAwayBadge,
                    FavoriteMatch.MATCH_DATE to match?.dateEvent,
                    FavoriteMatch.LEAGUE_ID to match?.idLeague,
                    FavoriteMatch.INT_ROUND to match?.intRound,
                    FavoriteMatch.AWAY_FORM to match?.strAwayFormation,
                    FavoriteMatch.AWAY_GOAL to match?.strAwayGoalDetails,
                    FavoriteMatch.AWAY_DEFENSE to match?.strAwayLineupDefense,
                    FavoriteMatch.AWAY_FORWARD to match?.strAwayLineupForward,
                    FavoriteMatch.AWAY_GK to match?.strAwayLineupGoalkeeper,
                    FavoriteMatch.AWAY_MID to match?.strAwayLineupMidfield,
                    FavoriteMatch.AWAY_SUB to match?.strAwayLineupSubstitutes,
                    FavoriteMatch.AWAY_RED to match?.strAwayRedCards,
                    FavoriteMatch.AWAY_YELLOW to match?.strAwayYellowCards,
                    FavoriteMatch.MATCH_NAME to match?.strEvent,
                    FavoriteMatch.HOME_FORM to match?.strHomeFormation,
                    FavoriteMatch.HOME_GOAL to match?.strHomeGoalDetails,
                    FavoriteMatch.HOME_DEFENSE to match?.strHomeLineupDefense,
                    FavoriteMatch.HOME_FORWARD to match?.strHomeLineupForward,
                    FavoriteMatch.HOME_GK to match?.strHomeLineupGoalkeeper,
                    FavoriteMatch.HOME_MID to match?.strHomeLineupMidfield,
                    FavoriteMatch.HOME_SUB to match?.strHomeLineupSubstitutes,
                    FavoriteMatch.HOME_RED to match?.strHomeRedCards,
                    FavoriteMatch.HOME_YELLOW to match?.strHomeYellowCards,
                    FavoriteMatch.LEAGUE_NAME to match?.strLeague,
                    FavoriteMatch.SEASON_NAME to match?.strSeason,
                    FavoriteMatch.SPORT_NAME to match?.strSport,
                    FavoriteMatch.TIME_STR to match?.strTime,
                    FavoriteMatch.VIDEO_STR to match?.strVideo
                )
            }
            toast("Added to Favorite")
            isFavorited = true
            showFavorite(isFavorited)
        } catch (e: SQLiteConstraintException) {
            toast(e.localizedMessage)
        }
    }

    private fun removeFromFavorite() {
        try {
            databaseMatch.use {
                delete(FavoriteMatch.TABLE_FAVORITE_MATCH, "(MATCH_ID = ${match?.idEvent})")
            }
            toast("Removed from Favorite")
            isFavorited = false
            showFavorite(isFavorited)
        } catch (e: SQLiteConstraintException) {
            toast(e.localizedMessage)
        }
    }

    private fun checkFavorite() {
        databaseMatch.use {
            select(FavoriteMatch.TABLE_FAVORITE_MATCH,FavoriteMatch.MATCH_ID).whereArgs("(${FavoriteMatch.MATCH_ID} = {matchId})", "matchId" to match?.idEvent!!).exec {
                if (count != 0) isFavorited = true
            }
            showFavorite(isFavorited)
        }
    }

    private fun showFavorite(isFavorited: Boolean) {
        if (isFavorited) btnLove.image = resources.getDrawable(R.drawable.ic_favorite_pink_28dp)
        else btnLove.image = resources.getDrawable(R.drawable.ic_favorite_border_white_24dp)
    }
}