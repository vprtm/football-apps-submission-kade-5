package project.kotlin.submissionkade2.activity

import android.app.ProgressDialog
import android.database.sqlite.SQLiteConstraintException
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.core.view.get
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_team.*
import org.jetbrains.anko.browse
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.toast
import project.kotlin.submissionkade2.Helper.databaseTeam
import project.kotlin.submissionkade2.Presenter.TeamPresenter
import project.kotlin.submissionkade2.R
import project.kotlin.submissionkade2.Repository.TeamsRepository
import project.kotlin.submissionkade2.adapter.RecycleGaleryAdapter
import project.kotlin.submissionkade2.models.*
import project.kotlin.submissionkade2.view.TeamView

class DetailTeamActivity : AppCompatActivity(),TeamView {

    private var itemGalery: MutableList<String> = mutableListOf()
    private var idTeam: Int? = null
    private var itemTeam : Team? = null
    private lateinit var teamPresenter : TeamPresenter
    private lateinit var mProgressDialog : ProgressDialog
    private lateinit var adapter: RecycleGaleryAdapter
    private var isLoved : Boolean = false
    private var menu : Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_team)
        idTeam = intent?.getIntExtra("idTeam",0)
        teamPresenter = TeamPresenter(this, TeamsRepository())

        val layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false)
        adapter = RecycleGaleryAdapter(itemGalery, applicationContext)
        recycler_view_galery.adapter = adapter
        recycler_view_galery.layoutManager = layoutManager

        setSupportActionBar(toolbarTeam)
        mProgressDialog =  indeterminateProgressDialog("Loading Team Data...")

        idTeam?.let { teamPresenter.getTeamDetail(it) }
    }

    private fun showTeamDetail(team : Team?){

        itemTeam = team
        team?.strStadiumThumb?.let { itemGalery.add(it) }
        team?.strTeamFanart1?.let { itemGalery.add(it) }
        team?.strTeamFanart2?.let { itemGalery.add(it) }
        team?.strTeamFanart3?.let { itemGalery.add(it) }
        team?.strTeamFanart4?.let { itemGalery.add(it) }
        team?.strTeamBanner?.let { itemGalery.add(it) }

        toolbarTeam.setTitle(team?.strTeam)

        Picasso.get().load(team?.strTeamBadge).into(ivDetailTeam)
        tvNameTeamDetail.text = team?.strTeam
        tvDescriptionTeam.text = team?.strDescriptionEN
        if(team?.strAlternate != null)tvAlternateNameTeam.text = team?.strAlternate
        else tvAlternateNameTeam.text = team?.strTeam

        if(team?.strWebsite != null)fabWebTeam.setOnClickListener { browse("https://${team?.strWebsite}",true) }
        else fabWebTeam.setOnClickListener { toast("Website not Available") }

        if(team?.strYoutube != null)fabYoutubeTeam.setOnClickListener { browse("https://${team?.strYoutube}",true) }
        else fabYoutubeTeam.setOnClickListener { toast("Youtube not Available") }

        if(team?.strFacebook != null)fabFacebookTeam.setOnClickListener { browse("https://${team?.strFacebook}",true) }
        else fabFacebookTeam.setOnClickListener { toast("Facebook not Available") }

        checkFavorite()

        adapter.notifyDataSetChanged()
        mProgressDialog.hide()
    }

    override fun onShowLoading() {
        mProgressDialog.show()
    }

    override fun onHideLoading() {
    }

    override fun onDataTeamsLoaded(data: TeamResponse?) {
        showTeamDetail(data?.listTeam?.get(0))
    }

    override fun onDataTeamsError() {
        toast("Failed Loaded Team Data")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        this.menu = menu
        menuInflater.inflate(R.menu.love, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        else if (item?.itemId == R.id.love){
            if (isLoved){
                item.icon = resources.getDrawable(R.drawable.ic_favorite_border_white_24dp)
                removeFromFavorite()
            }
            else{
                item.icon = resources.getDrawable(R.drawable.ic_favorite_pink_28dp)
                addToFavorite()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun removeFromFavorite() {
        try {
            databaseTeam.use {
                delete(FavoriteTeam.TABLE_FAVORITE_TEAM, "(TEAM_ID = ${idTeam})")
            }
            toast("Removed from Favorite")
            isLoved = false
            showFavorite(isLoved)
        } catch (e: SQLiteConstraintException) {
            toast(e.localizedMessage)
        }
    }

    private fun checkFavorite() {
        databaseTeam.use {
            select(
                FavoriteTeam.TABLE_FAVORITE_TEAM).whereArgs("(${FavoriteTeam.TEAM_ID} = {teamId})", "teamId" to idTeam!!).exec {
                if (count != 0) isLoved = true
            }
        }
        showFavorite(isLoved)
    }

    private fun addToFavorite() {
        try {
            databaseTeam.use {
                insert(
                    FavoriteTeam.TABLE_FAVORITE_TEAM,
                    FavoriteTeam.TEAM_ID to itemTeam?.idTeam,
                    FavoriteTeam.LEAGUE_ID to itemTeam?.idLeague,
                    FavoriteTeam.STR_TEAM to itemTeam?.strTeam,
                    FavoriteTeam.STR_LEAGUE to itemTeam?.strLeague,
                    FavoriteTeam.STR_DESC_EN to itemTeam?.strDescriptionEN,
                    FavoriteTeam.STR_TEAM_BADGE to itemTeam?.strTeamBadge
                )
            }
            toast("Added to Favorite")
            isLoved = true
            showFavorite(isLoved)
        } catch (e: SQLiteConstraintException) {
            toast(e.localizedMessage)
        }
    }

    private fun showFavorite(isFavorited: Boolean) {
        if (isFavorited) menu?.get(0)?.icon = resources.getDrawable(R.drawable.ic_favorite_pink_28dp)
        else menu?.get(0)?.icon = resources.getDrawable(R.drawable.ic_favorite_border_white_24dp)
    }
}
