package project.kotlin.submissionkade2.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_favorite.*
import kotlinx.android.synthetic.main.fragment_favorite.*
import project.kotlin.submissionkade2.R
import project.kotlin.submissionkade2.adapter.FavoritePagerAdapter
import project.kotlin.submissionkade2.adapter.RecycleTeamAdapter
import project.kotlin.submissionkade2.models.FavoriteTeam
import project.kotlin.submissionkade2.models.Match

class FavoriteMatchActivity : AppCompatActivity() {

    private var idLeague : Int? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite)

        idLeague = intent?.extras?.getInt("idLeague")
        pagerFavorite.adapter = idLeague?.let { FavoritePagerAdapter(supportFragmentManager, it) }

        setSupportActionBar(toolbarFavorite)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.no_animation, R.anim.slide_down_back);
    }

    override fun onResume() {
        super.onResume()
        pagerFavorite.adapter = idLeague?.let { FavoritePagerAdapter(supportFragmentManager, it) }
    }
}
