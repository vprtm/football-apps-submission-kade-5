package project.kotlin.submissionkade2.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_favorite_team.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import project.kotlin.submissionkade2.Helper.databaseTeam
import project.kotlin.submissionkade2.R
import project.kotlin.submissionkade2.adapter.RecycleFavTeamAdapter
import project.kotlin.submissionkade2.models.FavoriteTeam
import project.kotlin.submissionkade2.utils.gone
import project.kotlin.submissionkade2.utils.invisible
import project.kotlin.submissionkade2.utils.visible

class FavoriteTeamActivity : AppCompatActivity() {

    private var idLeague: Int? = null
    private var teams: MutableList<FavoriteTeam> = mutableListOf()
    private lateinit var adapter: RecycleFavTeamAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite_team)
        idLeague = intent?.getIntExtra("idLeague",0)
        setSupportActionBar(toolbarFavoriteTeam)

        val layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)
        adapter = RecycleFavTeamAdapter(teams, applicationContext)
        recycler_view_favTeam.adapter = adapter
        recycler_view_favTeam.layoutManager = layoutManager
        showFavoriteTeam()
    }

    private fun showFavoriteTeam() {
        databaseTeam.use {
            val result = select(FavoriteTeam.TABLE_FAVORITE_TEAM).whereArgs("(${FavoriteTeam.LEAGUE_ID} = {leagueId})", "leagueId" to idLeague!!)
            val favorite = result.parseList(classParser<FavoriteTeam>())
            teams.clear()
            teams.addAll(favorite)
            adapter.notifyDataSetChanged()
            if (teams != null){
                recycler_view_favTeam.visible()
                tvEmptyFavTeam.gone()
            }
            else{
                tvEmptyFavTeam.visible()
                recycler_view_favTeam.invisible()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.no_animation, R.anim.slide_down_back);
    }

    override fun onResume() {
        super.onResume()
        showFavoriteTeam()
    }
}
