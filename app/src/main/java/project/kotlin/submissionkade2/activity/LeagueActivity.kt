package project.kotlin.submissionkade2.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.viewpager.widget.ViewPager
import com.squareup.picasso.Picasso
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter
import project.kotlin.submissionkade2.R
import kotlinx.android.synthetic.main.activity_league.*
import org.jetbrains.anko.*
import project.kotlin.submissionkade2.adapter.ViewPagerAdapter
import project.kotlin.submissionkade2.models.FavoriteTeam
import project.kotlin.submissionkade2.models.League

class LeagueActivity : AppCompatActivity(){

    private var league : League? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_league)
        league = intent?.getParcelableExtra("data")
        var fabType = 0
        pagerView.adapter = ViewPagerAdapter(supportFragmentManager, league?.idLeague!!, league!!.strLeague.toString())
        pagerView.offscreenPageLimit = 3
        pagerView.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                if (position.equals(1)) speedDial.hide()
                else if (position.equals(0)){
                    speedDial.show()
                    fabType = 0
                }
                else{
                    speedDial.show()
                    fabType = 1
                }
            }
        })

        toolbarLeague.title = league?.strLeague
        setSupportActionBar(toolbarLeague)

        tvLeagueName.text = league?.strLeague
        tvLeagueDetail.text = league?.strDescriptionEN
        if(league?.strBadge != null) Picasso.get().load(league?.strBadge).into(ivLeague)

        cardDetail.setOnClickListener{
            startActivity<DetailLeagueActivity>("dataLeague" to league?.idLeague)
        }

        speedDial.setMenuListener(object : SimpleMenuListenerAdapter() {
            override fun onMenuItemSelected(menuItem: MenuItem?): Boolean {
                if (menuItem?.itemId?.equals(R.id.action_search_match)!!){
                    if (fabType.equals(0)){
                        startActivity<SearchMatchActivity>("dataName" to league?.strLeague)
                        overridePendingTransition(R.anim.right_bottom_up, R.anim.no_animation)
                    }
                    else{
                        startActivity<SearchTeamActivity>("idLeague" to league?.idLeague)
                        overridePendingTransition(R.anim.right_bottom_up, R.anim.no_animation)
                    }
                }
                else{
                    if (fabType.equals(0)){
                        startActivity<FavoriteMatchActivity>("idLeague" to league?.idLeague)
                        overridePendingTransition(R.anim.right_bottom_up, R.anim.no_animation)
                    }
                    else{
                        startActivity<FavoriteTeamActivity>("idLeague" to league?.idLeague)
                        overridePendingTransition(R.anim.right_bottom_up, R.anim.no_animation)
                    }
                }
                return super.onMenuItemSelected(menuItem)
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

}
