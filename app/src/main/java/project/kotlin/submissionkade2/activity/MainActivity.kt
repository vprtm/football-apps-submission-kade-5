package project.kotlin.submissionkade2.activity

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import project.kotlin.submissionkade2.R
import project.kotlin.submissionkade2.adapter.RecyclerMainAdapter
import project.kotlin.submissionkade2.models.League
import project.kotlin.submissionkade2.models.LeagueResponse
import project.kotlin.submissionkade2.service.EspressoIdlingResource
import project.kotlin.submissionkade2.utils.gone
import project.kotlin.submissionkade2.utils.visible
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast
import project.kotlin.submissionkade2.Presenter.LeaguePresenter
import project.kotlin.submissionkade2.Repository.LeagueRepository
import project.kotlin.submissionkade2.utils.invisible
import project.kotlin.submissionkade2.view.LeagueView

class MainActivity : AppCompatActivity(),LeagueView{

    private var leagues: MutableList<League> = mutableListOf()
    private lateinit var adapter: RecyclerMainAdapter
    private lateinit var leaguePresenter : LeaguePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = RecyclerMainAdapter(leagues,this)
        recycler_main.adapter = adapter
        recycler_main.layoutManager = GridLayoutManager(this,2, GridLayoutManager.VERTICAL,
            false)
        recycler_main.setHasFixedSize(true)

        leaguePresenter = LeaguePresenter(this, LeagueRepository())

        EspressoIdlingResource.increment()
        leaguePresenter.getLeague()
    }


    fun showLeagueList(data: List<League>) {
        if (!EspressoIdlingResource.idlingresource.isIdleNow) {
            EspressoIdlingResource.decrement()
        }
        leagues.clear()
        leagues.addAll(data)
        progressBarMain.gone()
        adapter.notifyDataSetChanged()
    }

    override fun onShowLoadingLeague() {
        progressBarMain.visible()
        recycler_main.invisible()
    }

    override fun onHideLoadingLeague() {
        recycler_main.visible()
    }

    override fun onDataLeagueLoaded(data: LeagueResponse?) {
        data?.listLeague?.let { showLeagueList(it) }
    }

    override fun onDataLeagueError() {
        toast("Failed Load League Data")
    }
}
