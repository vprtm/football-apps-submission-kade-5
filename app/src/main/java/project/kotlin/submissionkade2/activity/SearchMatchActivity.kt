package project.kotlin.submissionkade2.activity

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.SearchEvent
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import project.kotlin.submissionkade2.R
import kotlinx.android.synthetic.main.activity_search_match.*
import org.jetbrains.anko.toast
import project.kotlin.submissionkade2.Presenter.SearchEventPresenter
import project.kotlin.submissionkade2.Repository.EventRepository
import project.kotlin.submissionkade2.adapter.RecyclerMatchAdapter
import project.kotlin.submissionkade2.models.*
import project.kotlin.submissionkade2.service.DataRepository
import project.kotlin.submissionkade2.service.EspressoIdlingResource
import project.kotlin.submissionkade2.utils.gone
import project.kotlin.submissionkade2.utils.invisible
import project.kotlin.submissionkade2.utils.visible
import project.kotlin.submissionkade2.view.SearchEventView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SearchMatchActivity : AppCompatActivity(), SearchEventView {

    private var itemEvent: MutableList<EventItem> = mutableListOf()
    private var match: MutableList<Match> = mutableListOf(Match(null, null, null,null, null, null, null, null, null, null, null,null, null, null,null, null, null,null, null, null,null, null, null,null, null, null,null, null, null,null, null, null,null, null, null,null, null))
    private var teams: MutableMap<Int?, Teams> = mutableMapOf()
    private lateinit var adapter: RecyclerMatchAdapter
    private var strLeague: String? = null
    private lateinit var searchEventPresenter : SearchEventPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_match)

        strLeague = intent.getStringExtra("dataName")
        intent.putExtra("dataName",strLeague)
        handleIntent(intent)
        setSupportActionBar(toolbarSearch)

        val layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)
        adapter = RecyclerMatchAdapter(match, applicationContext, teams)
        recycler_view_search.adapter = adapter
        recycler_view_search.layoutManager = layoutManager

        searchEventPresenter = SearchEventPresenter(this, EventRepository())
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.no_animation, R.anim.slide_down_back);
    }

    private fun doSearch(query: String) {
        tvEmptySearch.gone()
        recycler_view_search.invisible()
        EspressoIdlingResource.increment()
        searchEventPresenter.getSearch(query)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
        if (intent != null) {
            handleIntent(intent)
        }
    }

    private fun handleIntent(intent: Intent) {
        if (Intent.ACTION_SEARCH == intent.action) {
            intent.getStringExtra(SearchManager.QUERY)?.also { query ->
                doSearch(query)
            }
        }
    }

    override fun onSearchRequested(searchEvent: SearchEvent?): Boolean {
        val appData = Bundle().apply {
            putBoolean("booleanSearch", true)
        }
        startSearch(null, false, appData, false)
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.search, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu?.findItem(R.id.search)?.actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            setIconifiedByDefault(false)
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    fun showEmpty() {
        progressBarSearch.gone()
        recycler_view_search.gone()
        tvEmptySearch.visible()
    }

    fun showMatchList(data: MutableList<Match>) {
        if (!EspressoIdlingResource.idlingresource.isIdleNow) {
            EspressoIdlingResource.decrement()
        }
        if(data.get(0).idEvent != null){
            progressBarSearch.gone()
            match.removeAt(match.lastIndex)
            adapter.notifyDataSetChanged()
            recycler_view_search.visible()
        }
        else showEmpty()


    }

    fun addTeam(teamList: List<Teams>) {
        val iterator = teamList.listIterator()
        for (item in iterator) {
            teams.put(item.idTeam, item)
        }
        addMatchList()
    }

    fun addMatchList() {
        val iterator = itemEvent.listIterator()
        var index = 0
        for (item in iterator) {
            if(item.strLeague == strLeague) {
                val matchItem = Match(
                    item.idEvent,
                    item.strHomeTeam,
                    item.strAwayTeam,
                    item.intHomeScore,
                    item.intAwayScore,
                    item.idHomeTeam,
                    item.idAwayTeam,
                    item.dateEvent,
                    teams.get(item.idAwayTeam)?.strTeamBadge,
                    teams.get(item.idHomeTeam)?.strTeamBadge,
                    item.dateEvent,
                    item.idLeague,
                    item.intRound,
                    item.strAwayFormation,
                    item.strAwayGoalDetails,
                    item.strAwayLineupDefense,
                    item.strAwayLineupForward,
                    item.strAwayLineupGoalkeeper,
                    item.strAwayLineupMidfield,
                    item.strAwayLineupSubstitutes,
                    item.strAwayRedCards,
                    item.strAwayYellowCards,
                    item.strEvent,
                    item.strHomeFormation,
                    item.strHomeGoalDetails,
                    item.strHomeLineupDefense,
                    item.strHomeLineupForward,
                    item.strHomeLineupGoalkeeper,
                    item.strHomeLineupMidfield,
                    item.strHomeLineupSubstitutes,
                    item.strHomeRedCards,
                    item.strHomeYellowCards,
                    item.strLeague,
                    item.strSeason,
                    item.strSport,
                    item.strTime,
                    item.strVideo
                )
                match.add(index, matchItem)
                index++
            }

        }

        showMatchList(match)
    }

    fun getTeamList() {
        val teamServices = DataRepository.createTeams()
        teamServices.getTeam(strLeague).enqueue(object : Callback<TeamsResponse> {
            override fun onResponse(call: Call<TeamsResponse>, response: Response<TeamsResponse>) {
                if (response.isSuccessful) {
                    response.body()?.listTeam?.let { addTeam(it) }
                }
            }

            override fun onFailure(call: Call<TeamsResponse>, error: Throwable) {
                Log.e("tag", "errornya ${error.message}")
            }
        })
    }

    override fun onShowLoadingSearch() {
        progressBarSearch.visible()
        recycler_view_search.invisible()
        tvEmptySearch.gone()
    }

    override fun onHideLoadingSearch() {
    }

    override fun onDataLoadedEvent(data: SearchResponse?) {
        if(data?.listEvent != null){
            itemEvent = data?.listEvent as MutableList<EventItem>
            getTeamList()
        }
        else showEmpty()
    }

    override fun onDataErrorEvent() {
        toast("Failed Loading Match Data")
    }
}


