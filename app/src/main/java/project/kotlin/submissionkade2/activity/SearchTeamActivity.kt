package project.kotlin.submissionkade2.activity

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.SearchEvent
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_search_team.*
import org.jetbrains.anko.toast
import project.kotlin.submissionkade2.Presenter.TeamPresenter
import project.kotlin.submissionkade2.R
import project.kotlin.submissionkade2.Repository.TeamsRepository
import project.kotlin.submissionkade2.adapter.RecycleTeamAdapter
import project.kotlin.submissionkade2.models.*
import project.kotlin.submissionkade2.utils.gone
import project.kotlin.submissionkade2.utils.invisible
import project.kotlin.submissionkade2.utils.visible
import project.kotlin.submissionkade2.view.TeamView

class SearchTeamActivity : AppCompatActivity(),TeamView {

    private var itemTeam: MutableList<Team> = mutableListOf()
    private lateinit var adapter: RecycleTeamAdapter
    private var idLeague: Int? = null
    private lateinit var searchTeamPresenter : TeamPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_team)

        idLeague = intent?.getIntExtra("idLeague",0)
        intent.putExtra("idLeague",idLeague)
        handleIntent(intent)
        setSupportActionBar(toolbarSearchTeam)

        val layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)
        adapter = RecycleTeamAdapter(itemTeam, applicationContext)
        recycler_view_searchTeam.adapter = adapter
        recycler_view_searchTeam.layoutManager = layoutManager

        searchTeamPresenter = TeamPresenter(this, TeamsRepository())
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.no_animation, R.anim.slide_down_back);
    }

    private fun doMySearch(query: String) {
        tvEmptySearchTeam.gone()
        recycler_view_searchTeam.invisible()
        searchTeamPresenter.searchTeam(query)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
        if (intent != null) {
            handleIntent(intent)
        }
    }

    private fun handleIntent(intent: Intent) {
        if (Intent.ACTION_SEARCH == intent.action) {
            intent.getStringExtra(SearchManager.QUERY)?.also { query ->
                doMySearch(query)
            }
        }
    }

    override fun onSearchRequested(searchEvent: SearchEvent?): Boolean {
        val appData = Bundle().apply {
            putBoolean("booleanSearchTeam", true)
        }
        startSearch(null, false, appData, false)
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.search, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu?.findItem(R.id.search)?.actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            setIconifiedByDefault(false)
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    fun showEmpty() {
        progressBarSearchTeam.gone()
        recycler_view_searchTeam.gone()
        tvEmptySearchTeam.visible()
    }

    override fun onShowLoading() {
        progressBarSearchTeam.visible()
        recycler_view_searchTeam.invisible()
        tvEmptySearchTeam.gone()
    }

    override fun onHideLoading() {
    }

    override fun onDataTeamsLoaded(data: TeamResponse?) {
        val list = data?.listTeam?.filter {
            it.idLeague.equals(idLeague)
        }
        if(list?.size != 0){
            list?.let { showTeamList(it) }
        }
        else showEmpty()
    }

    private fun showTeamList(data : List<Team>) {
        itemTeam.addAll(data)
        progressBarSearchTeam.gone()
        adapter.notifyDataSetChanged()
        recycler_view_searchTeam.visible()
    }

    override fun onDataTeamsError() {
        toast("Failed Loading Team Data")
    }
}
