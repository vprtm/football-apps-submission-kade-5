package project.kotlin.submissionkade2.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import project.kotlin.submissionkade2.Fragment.FavoriteFragment

class FavoritePagerAdapter(fm: FragmentManager, idLeague : Int): FragmentPagerAdapter(fm){

    private val pages : List<Fragment> = listOf(
        FavoriteFragment.newFragment(idLeague,0),
        FavoriteFragment.newFragment(idLeague,1)
    )

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getCount(): Int {
        return pages.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "Previous Match"
            else -> "Next Match"
        }
    }
}