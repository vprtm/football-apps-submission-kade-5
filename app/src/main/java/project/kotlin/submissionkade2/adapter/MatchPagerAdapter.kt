package project.kotlin.submissionkade2.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import project.kotlin.submissionkade2.Fragment.NextFragment
import project.kotlin.submissionkade2.Fragment.PreviousFragment
import project.kotlin.submissionkade2.models.League

class MatchPagerAdapter(fm: FragmentManager, idLeague : Int, strLeague : String?): FragmentPagerAdapter(fm){

    private val pages : List<Fragment> = listOf(
        PreviousFragment.newFragment(idLeague, strLeague),
        NextFragment.newFragment(idLeague, strLeague)
    )

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getCount(): Int {
        return pages.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "Previous Match"
            else -> "Next Match"
        }
    }
}