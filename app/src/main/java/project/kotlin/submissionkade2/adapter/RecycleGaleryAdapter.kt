package project.kotlin.submissionkade2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.recycle_item_galery.view.*
import kotlinx.android.synthetic.main.recycle_item_team.view.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask
import project.kotlin.submissionkade2.R
import project.kotlin.submissionkade2.activity.DetailTeamActivity
import project.kotlin.submissionkade2.models.Teams
import project.kotlin.submissionkade2.utils.gone
import project.kotlin.submissionkade2.utils.visible
import java.lang.Exception

class RecycleGaleryAdapter(private val items: List<String>, val context : Context?)
    : RecyclerView.Adapter<RecycleGaleryAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.recycle_item_galery, parent, false))
    }

    class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
        LayoutContainer {
        fun bindItem(items: String) {
            with(itemView) {
                Picasso.get().load(items).into(ivItemGalery, object : Callback{
                    override fun onSuccess() {
                        ivItemGalery.visible()
                        progress_ivItemGalery.gone()
                    }

                    override fun onError(e: Exception?) {
                        progress_ivItemGalery.gone()
                        ivItemGalery.visible()
                        ivItemGalery.setImageResource(R.drawable.not_found)
                    }
                })
            }
        }

    }
    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position])
    }
}