package project.kotlin.submissionkade2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.recycle_item_klasemen.view.*
import project.kotlin.submissionkade2.R
import project.kotlin.submissionkade2.models.Klasemen
import project.kotlin.submissionkade2.utils.gone
import project.kotlin.submissionkade2.utils.visible
import java.lang.Exception

class RecycleStandingAdapter(private val items: List<Klasemen>, val context : Context?)
    : RecyclerView.Adapter<RecycleStandingAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.recycle_item_klasemen, parent, false))
    }

    class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
        LayoutContainer {
        fun bindItem(items: Klasemen) {
            with(itemView) {
                klasemenTeam.text = items.name
                klasemenPlays.text = items.played.toString()
                klasemenGoals.text = items.goalsfor.toString()
                klasemenWins.text = items.win.toString()
                klasemenDraws.text = items.draw.toString()
                klasemenLoses.text = items.loss.toString()

                Picasso.get().load(items.strLogo).into(ivItemKlasemen, object : Callback {
                    override fun onSuccess() {
                        ivItemKlasemen.visible()
                        progress_ivItemKlasemen.gone()
                    }

                    override fun onError(e: Exception?) {
                        progress_ivItemKlasemen.gone()
                        ivItemKlasemen.visible()
                        ivItemKlasemen.setImageResource(R.drawable.not_found)
                    }
                })
            }
        }

    }
    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position])
    }
}