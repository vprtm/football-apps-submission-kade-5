package project.kotlin.submissionkade2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.recycle_item_team.view.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask
import project.kotlin.submissionkade2.R
import project.kotlin.submissionkade2.activity.DetailTeamActivity
import project.kotlin.submissionkade2.models.Team
import project.kotlin.submissionkade2.utils.gone
import project.kotlin.submissionkade2.utils.visible
import java.lang.Exception

class RecycleTeamAdapter(private val items: MutableList<Team>, val context: Context?)
    : RecyclerView.Adapter<RecycleTeamAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.recycle_item_team, parent, false))
    }

    class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
        LayoutContainer {
        fun bindItem(items: Team) {
            with(itemView) {
                tvItemTeamsName.text = items.strTeam
                tvItemTeamsDesc.text = items.strDescriptionEN
                Picasso.get().load(items.strTeamBadge).into(ivItemTeams, object : Callback {
                    override fun onSuccess() {
                        ivItemTeams.visible()
                        progress_ivItemTeams.gone()
                    }

                    override fun onError(e: Exception?) {
                        progress_ivItemTeams.gone()
                        ivItemTeams.visible()
                        ivItemTeams.setImageResource(R.drawable.not_found)
                    }
                })

                containerView.setOnClickListener{
                    containerView.context.startActivity(containerView.context.intentFor<DetailTeamActivity>("idTeam" to items.idTeam).newTask())
                }
            }
        }

    }
    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position])
    }
}