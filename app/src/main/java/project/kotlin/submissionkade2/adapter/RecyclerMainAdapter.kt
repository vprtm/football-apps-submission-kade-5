package project.kotlin.submissionkade2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.extensions.LayoutContainer
import org.jetbrains.anko.*
import project.kotlin.submissionkade2.R
import project.kotlin.submissionkade2.activity.LeagueActivity
import project.kotlin.submissionkade2.models.League
import kotlinx.android.synthetic.main.recycle_item_main.view.*
import project.kotlin.submissionkade2.utils.gone
import project.kotlin.submissionkade2.utils.visible
import java.lang.Exception

class RecyclerMainAdapter(private val items: List<League>, val context : Context?)
    : RecyclerView.Adapter<RecyclerMainAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.recycle_item_main, parent, false))
    }

    class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
        LayoutContainer {
        fun bindItem(items: League) {
            with(itemView) {
                tvItemLeague.text = items.strLeague
                if(items.strLogo != null)Picasso.get().load(items.strLogo).into(ivItemLeague, object : Callback{
                    override fun onSuccess() {
                        ivItemLeague.visible()
                        progress_ivItemLeague.gone()
                    }

                    override fun onError(e: Exception?) {
                        progress_ivItemLeague.gone()
                        ivItemLeague.visible()
                        ivItemLeague.setImageResource(R.drawable.not_found)
                    }
                })
                containerView.setOnClickListener{
                    containerView.context.toast("You choose ${items.strLeague}")
                    containerView.context.startActivity(containerView.context.intentFor<LeagueActivity>("data" to items).newTask())
                }
            }
        }

    }
    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position])
    }
}