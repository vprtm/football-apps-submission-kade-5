package project.kotlin.submissionkade2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.extensions.LayoutContainer
import project.kotlin.submissionkade2.R
import project.kotlin.submissionkade2.models.Match
import kotlinx.android.synthetic.main.recycle_item_match.view.*
import project.kotlin.submissionkade2.activity.DetailMatchActivity
import project.kotlin.submissionkade2.models.Teams
import org.jetbrains.anko.*
import project.kotlin.submissionkade2.utils.gone
import project.kotlin.submissionkade2.utils.visible
import java.lang.Exception

class RecyclerMatchAdapter(private val matchList: MutableList<Match> = mutableListOf(),
                           val context : Context?, val teams : MutableMap<Int?, Teams>)
    : RecyclerView.Adapter<RecyclerMatchAdapter.MatchViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchViewHolder {
        return MatchViewHolder(LayoutInflater.from(context).inflate(R.layout.recycle_item_match, parent, false))
    }

    class MatchViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
        LayoutContainer {
        fun bindItem(items: Match, teams : MutableMap<Int?, Teams>) {
            with(itemView) {
                tvNameA.text = items.strHomeTeam
                tvNameB.text = items.strAwayTeam
                if (items.intAwayScore != null) {
                    tvSkorA.text = items.intHomeScore.toString()
                    tvSkorB.text = items.intAwayScore.toString()
                }
                tvDate.text = items.dateEvent
                tvName.text = items.strEvent
                Picasso.get().load(items.strAwayBadge).into(imageViewB,  object : Callback {
                    override fun onSuccess() {
                        imageViewB.visible()
                        progress_ivRecycleB.gone()
                    }

                    override fun onError(e: Exception?) {
                        progress_ivRecycleB.gone()
                        imageViewB.visible()
                        imageViewB.setImageResource(R.drawable.not_found)
                    }
                })
                Picasso.get().load(items.strHomeBadge).into(imageViewA, object : Callback {
                    override fun onSuccess() {
                        imageViewA.visible()
                        progress_ivRecycleA.gone()
                    }

                    override fun onError(e: Exception?) {
                        progress_ivRecycleA.gone()
                        imageViewA.visible()
                        imageViewA.setImageResource(R.drawable.not_found)
                    }
                })
            }

            containerView.setOnClickListener{
                containerView.context.startActivity(containerView.context.intentFor<DetailMatchActivity>("match" to items, "teams" to teams).newTask())
            }
        }

    }
    override fun getItemCount(): Int = matchList.size

    override fun onBindViewHolder(holder: MatchViewHolder, position: Int) {
        holder.bindItem(matchList[position],teams)
    }
}