package project.kotlin.submissionkade2.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import io.github.yavski.fabspeeddial.FabSpeedDial
import project.kotlin.submissionkade2.Fragment.*

class ViewPagerAdapter(fm: FragmentManager, idLeague: Int, strLeague: String): FragmentPagerAdapter(fm) {

    private val pages: List<Fragment> = listOf(
        MatchFragment.newFragment(idLeague,strLeague),
        StandingFragment.newFragment(idLeague,strLeague),
        TeamFragment.newFragment(idLeague,strLeague)
    )

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getCount(): Int {
        return pages.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Matchs"
            1 -> "Standings"
            else -> "Teams"
        }
    }
}