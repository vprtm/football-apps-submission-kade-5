package project.kotlin.submissionkade2.`interface`

import project.kotlin.submissionkade2.models.EventResponse
import project.kotlin.submissionkade2.models.SearchResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface EventServices {

    @GET("eventsnextleague.php")
    fun getNext(@Query("id") id : Int?): Call<EventResponse>

    @GET("eventspastleague.php")
    fun getPrevious(@Query("id") id:Int?) : Call<EventResponse>

    @GET("searchevents.php")
    fun searchEvent(@Query("e") query: String?) : Call<SearchResponse>

    @GET("lookupevent.php")
    fun detailEvent(@Query("id") id:Int?) : Call<EventResponse>

}