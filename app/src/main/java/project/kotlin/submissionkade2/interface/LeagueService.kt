package project.kotlin.submissionkade2.`interface`

import project.kotlin.submissionkade2.models.DetailLeagueResponse
import project.kotlin.submissionkade2.models.LeagueResponse
import project.kotlin.submissionkade2.models.StandingResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface LeagueServices {

    @GET("search_all_leagues.php?s=Soccer")
    fun getPosts(): Call<LeagueResponse>

    @GET("lookupleague.php")
    fun searchLeague(@Query("id") query: Int?) : Call<DetailLeagueResponse>

    @GET("lookuptable.php?")
    fun getStanding(@Query("l") id: Int?) : Call<StandingResponse>
}