package project.kotlin.submissionkade2.`interface`

import project.kotlin.submissionkade2.models.TeamResponse
import project.kotlin.submissionkade2.models.TeamsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface TeamServices {

    @GET("search_all_teams.php")
    fun getTeam(@Query("l") name : String?): Call<TeamsResponse>

    @GET("lookupteam.php")
    fun getTeamDetail(@Query("id") id : Int?): Call<TeamResponse>

    @GET("searchteams.php")
    fun searchTeam(@Query("t") query : String?): Call<TeamResponse>

}