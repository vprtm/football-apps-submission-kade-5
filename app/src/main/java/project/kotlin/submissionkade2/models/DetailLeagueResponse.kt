package project.kotlin.submissionkade2.models

import com.google.gson.annotations.SerializedName
data class DetailLeagueResponse(@SerializedName("leagues") val listLeague: List<League>)