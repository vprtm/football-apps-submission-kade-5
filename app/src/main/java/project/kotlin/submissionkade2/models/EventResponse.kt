package project.kotlin.submissionkade2.models

import com.google.gson.annotations.SerializedName

data class EventResponse(@SerializedName("events") val listEvent: List<EventItem>)