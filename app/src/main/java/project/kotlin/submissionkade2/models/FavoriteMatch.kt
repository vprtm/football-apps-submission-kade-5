package project.kotlin.submissionkade2.models

data class FavoriteMatch(

    val id: Int?,
    val idEvent: Int?,
    val strHomeTeam: String?,
    val strAwayTeam: String?,
    val intHomeScore: Int?,
    val intAwayScore: Int?,
    val idHomeTeam: Int?,
    val idAwayTeam: Int?,
    val strDate: String?,
    val strAwayBadge: String?,
    val strHomeBadge: String?,
    val dateEvent: String?,
    val idLeague: Int?,
    val intRound: Int?,
    val strAwayFormation: String?,
    val strAwayGoalDetails: String?,
    val strAwayLineupDefense: String?,
    val strAwayLineupForward: String?,
    val strAwayLineupGoalkeeper: String?,
    val strAwayLineupMidfield: String?,
    val strAwayLineupSubstitutes: String?,
    val strAwayRedCards: String?,
    val strAwayYellowCards: String?,
    val strEvent: String?,
    val strHomeFormation: String?,
    val strHomeGoalDetails: String?,
    val strHomeLineupDefense: String?,
    val strHomeLineupForward: String?,
    val strHomeLineupGoalkeeper: String?,
    val strHomeLineupMidfield: String?,
    val strHomeLineupSubstitutes: String?,
    val strHomeRedCards: String?,
    val strHomeYellowCards: String?,
    val strLeague: String?,
    val strSeason: String?,
    val strSport: String?,
    val strTime: String?,
    val strVideo: String?

) {
    companion object {
        const val TABLE_FAVORITE_MATCH: String = "TABLE_FAVORITE_MATCH"
        const val ID: String = "ID_"
        const val MATCH_ID: String = "MATCH_ID"
        const val HOME_NAME: String = "HOME_NAME"
        const val AWAY_NAME: String = "AWAY_NAME"
        const val HOME_SCORE: String = "HOME_SCORE"
        const val AWAY_SCORE: String = "AWAY_SCORE"
        const val HOME_ID: String = "HOME_ID"
        const val AWAY_ID: String = "AWAY_ID"
        const val STR_DATE: String = "STR_DATE"
        const val HOME_BADGE: String = "HOME_BADGE"
        const val AWAY_BADGE: String = "AWAY_BADGE"
        const val MATCH_DATE: String = "MATCH_DATE"
        const val LEAGUE_ID: String = "LEAGUE_ID"
        const val INT_ROUND : String = "INT_ROUND"
        const val AWAY_FORM: String = "AWAY_FORM"
        const val AWAY_GOAL: String = "AWAY_GOAL"
        const val AWAY_DEFENSE: String = "AWAY_DEFENSE"
        const val AWAY_FORWARD: String = "AWAY_FORWARD"
        const val AWAY_GK: String = "AWAY_GK"
        const val AWAY_MID: String = "AWAY_MID"
        const val AWAY_SUB: String = "AWAY_SUB"
        const val AWAY_RED: String = "AWAY_RED"
        const val AWAY_YELLOW: String = "AWAY_YELLOW"
        const val MATCH_NAME: String = "MATCH_NAME"
        const val HOME_FORM: String = "HOME_FORM"
        const val HOME_GOAL: String = "HOME_GOAL"
        const val HOME_DEFENSE: String = "HOME_DEFENSE"
        const val HOME_FORWARD: String = "HOME_FORWARD"
        const val HOME_GK: String = "HOME_GK"
        const val HOME_MID: String = "HOME_MID"
        const val HOME_SUB: String = "HOME_SUB"
        const val HOME_RED: String = "HOME_RED"
        const val HOME_YELLOW: String = "HOME_YELLOW"
        const val LEAGUE_NAME: String = "LEAGUE_NAME"
        const val SEASON_NAME: String = "SEASON_NAME"
        const val SPORT_NAME: String = "SPORT_NAME"
        const val TIME_STR: String = "TIME_STR"
        const val VIDEO_STR: String = "VIDEO_STR"
    }
}