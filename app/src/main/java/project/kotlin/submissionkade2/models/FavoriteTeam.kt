package project.kotlin.submissionkade2.models

data class FavoriteTeam(
    val id: Int?,
    val idTeam: Int,
    val idLeague : Int,
    val strTeam : String,
    val strLeague : String,
    val strDescriptionEN : String,
    val strTeamBadge : String
){
    companion object{
        const val TABLE_FAVORITE_TEAM: String = "TABLE_FAVORITE_TEAMM"
        const val ID: String = "ID_"
        const val TEAM_ID = "TEAM_ID"
        const val STR_TEAM = "STR_TEAM"
        const val STR_LEAGUE = "STR_LEAGUE"
        const val LEAGUE_ID = "LEAGUE_ID"
        const val STR_DESC_EN = "STR_DESC_EN"
        const val STR_TEAM_BADGE = "STR_TEAM_BADGE"
    }
}
