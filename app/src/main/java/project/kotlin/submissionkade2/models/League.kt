package project.kotlin.submissionkade2.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class League(
    val idLeague: Int?,
    val strLeague: String?,
    val strLogo: String?,
    val strDescriptionEN: String?,
    val intFormedYear : Int?,
    val strBadge : String?,
    val strYoutube: String?,
    val strWebsite: String?,
    val strFacebook: String?

) : Parcelable