package project.kotlin.submissionkade2.models

import com.google.gson.annotations.SerializedName
data class LeagueResponse(@SerializedName("countrys") val listLeague: List<League>)