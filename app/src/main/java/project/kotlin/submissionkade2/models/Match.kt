package project.kotlin.submissionkade2.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Match(

    val idEvent : Int?,
    val strHomeTeam : String?,
    val strAwayTeam : String?,
    val intHomeScore : Int?,
    val intAwayScore : Int?,
    val idHomeTeam : Int?,
    val idAwayTeam : Int?,
    val strDate : String?,
    val strAwayBadge : String?,
    val strHomeBadge : String?,
    val dateEvent: String?,
    val idLeague: Int?,
    val intRound: Int?,
    val strAwayFormation: String?,
    val strAwayGoalDetails: String?,
    val strAwayLineupDefense: String?,
    val strAwayLineupForward: String?,
    val strAwayLineupGoalkeeper: String?,
    val strAwayLineupMidfield: String?,
    val strAwayLineupSubstitutes: String?,
    val strAwayRedCards: String?,
    val strAwayYellowCards: String?,
    val strEvent: String?,
    val strHomeFormation: String?,
    val strHomeGoalDetails: String?,
    val strHomeLineupDefense: String?,
    val strHomeLineupForward: String?,
    val strHomeLineupGoalkeeper: String?,
    val strHomeLineupMidfield: String?,
    val strHomeLineupSubstitutes: String?,
    val strHomeRedCards: String?,
    val strHomeYellowCards: String?,
    val strLeague: String?,
    val strSeason: String?,
    val strSport: String?,
    val strTime: String?,
    val strVideo: String?

) : Parcelable