package project.kotlin.submissionkade2.models

import com.google.gson.annotations.SerializedName

data class SearchResponse(@SerializedName("event") val listEvent: List<EventItem>)