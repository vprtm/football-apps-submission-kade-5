package project.kotlin.submissionkade2.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class StandingItem(
    val name : String?,
    val teamid : Int?,
    val played : Int?,
    val goalsfor : Int?,
    val goalsagainst : Int?,
    val goalsdifference : Int?,
    val win : Int?,
    val draw : Int?,
    val loss : Int?,
    val total : Int?

) : Parcelable