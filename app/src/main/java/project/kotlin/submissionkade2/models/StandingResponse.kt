package project.kotlin.submissionkade2.models

import com.google.gson.annotations.SerializedName

data class StandingResponse(@SerializedName("table") val listStanding: List<StandingItem>)