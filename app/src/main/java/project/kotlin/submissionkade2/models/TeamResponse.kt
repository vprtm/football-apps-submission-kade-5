package project.kotlin.submissionkade2.models

import com.google.gson.annotations.SerializedName
data class TeamResponse(@SerializedName("teams") val listTeam: List<Team>)