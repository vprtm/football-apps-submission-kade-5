package project.kotlin.submissionkade2.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Teams(
    val idLeague: Int?,
    val idTeam: Int,
    val intFormedYear: Int?,
    val intLoved: Int?,
    val intStadiumCapacity: Int?,
    val strAlternate: String?,
    val strCountry: String?,
    val strDescriptionEN: String?,
    val strDivision: String?,
    val strFacebook: String?,
    val strInstagram: String?,
    val strKeywords: String?,
    val strLeague: String?,
    val strManager: String?,
    val strRSS: String?,
    val strSport: String?,
    val strStadium: String?,
    val strStadiumDescription: String?,
    val strStadiumLocation: String?,
    val strStadiumThumb: String?,
    val strTeam: String?,
    val strTeamBadge: String,
    val strTeamBanner: String?,
    val strTeamFanart1: String?,
    val strTeamFanart2: String?,
    val strTeamFanart3: String?,
    val strTeamFanart4: String?,
    val strTeamJersey: String?,
    val strTeamLogo: String?,
    val strTwitter: String?,
    val strWebsite: String?,
    val strYoutube: String?
) : Parcelable
