package project.kotlin.submissionkade2.models

import com.google.gson.annotations.SerializedName
data class TeamsResponse(@SerializedName("teams") val listTeam: List<Teams>)