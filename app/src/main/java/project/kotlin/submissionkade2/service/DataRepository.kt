package project.kotlin.submissionkade2.service

import project.kotlin.submissionkade2.`interface`.EventServices
import project.kotlin.submissionkade2.`interface`.LeagueServices
import project.kotlin.submissionkade2.`interface`.TeamServices
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object DataRepository {

    fun createLeague(): LeagueServices {
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://www.thesportsdb.com/api/v1/json/1/")
            .build()
        return retrofit.create(LeagueServices::class.java)
    }

    fun createEvent(): EventServices {
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://www.thesportsdb.com/api/v1/json/1/")
            .build()
        return retrofit.create(EventServices::class.java)
    }

    fun createTeams(): TeamServices {
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://www.thesportsdb.com/api/v1/json/1/")
            .build()
        return retrofit.create(TeamServices::class.java)
    }
}