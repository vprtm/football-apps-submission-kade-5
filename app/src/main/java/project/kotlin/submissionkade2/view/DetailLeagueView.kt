package project.kotlin.submissionkade2.view

import project.kotlin.submissionkade2.Repository.LeagueRepositoryCallback
import project.kotlin.submissionkade2.models.DetailLeagueResponse

interface DetailLeagueView : LeagueRepositoryCallback<DetailLeagueResponse> {
    fun onShowLoadingDetailLeague()
    fun onHideLoadingDetailLeague()
}