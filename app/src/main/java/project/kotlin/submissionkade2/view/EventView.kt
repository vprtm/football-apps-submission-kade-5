package project.kotlin.submissionkade2.view

import project.kotlin.submissionkade2.Repository.EventRepositoryCallback
import project.kotlin.submissionkade2.models.EventResponse

interface EventView: EventRepositoryCallback<EventResponse>{
    fun onShowLoadingEvent()
    fun onHideLoadingEvent()
}