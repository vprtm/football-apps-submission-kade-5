package project.kotlin.submissionkade2.view

import project.kotlin.submissionkade2.Repository.LeagueRepositoryCallback
import project.kotlin.submissionkade2.models.LeagueResponse

interface LeagueView : LeagueRepositoryCallback<LeagueResponse>{
    fun onShowLoadingLeague()
    fun onHideLoadingLeague()
}