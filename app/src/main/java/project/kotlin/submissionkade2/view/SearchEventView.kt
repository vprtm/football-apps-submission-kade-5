package project.kotlin.submissionkade2.view

import project.kotlin.submissionkade2.Repository.EventRepositoryCallback
import project.kotlin.submissionkade2.models.SearchResponse

interface SearchEventView: EventRepositoryCallback<SearchResponse> {
    fun onShowLoadingSearch()
    fun onHideLoadingSearch()
}