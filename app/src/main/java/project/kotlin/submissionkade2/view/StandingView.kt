package project.kotlin.submissionkade2.view

import project.kotlin.submissionkade2.Repository.LeagueRepositoryCallback
import project.kotlin.submissionkade2.models.StandingResponse

interface StandingView: LeagueRepositoryCallback<StandingResponse> {
    fun onShowLoading()
    fun onHideLoading()
}