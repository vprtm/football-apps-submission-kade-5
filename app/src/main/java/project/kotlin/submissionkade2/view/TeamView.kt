package project.kotlin.submissionkade2.view

import project.kotlin.submissionkade2.Repository.TeamsRepositoryCallback
import project.kotlin.submissionkade2.models.TeamResponse

interface TeamView: TeamsRepositoryCallback<TeamResponse> {
    fun onShowLoading()
    fun onHideLoading()
}