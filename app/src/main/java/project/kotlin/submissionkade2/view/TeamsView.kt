package project.kotlin.submissionkade2.view

import project.kotlin.submissionkade2.Repository.TeamsRepositoryCallback
import project.kotlin.submissionkade2.models.TeamsResponse

interface TeamsView: TeamsRepositoryCallback<TeamsResponse>{
    fun onShowLoading()
    fun onHideLoading()
}