package project.kotlin.submissionkade2.presenterTest

import com.nhaarman.mockito_kotlin.argumentCaptor
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import project.kotlin.submissionkade2.Presenter.DetailLeaguePresenter
import project.kotlin.submissionkade2.Repository.LeagueRepository
import project.kotlin.submissionkade2.Repository.LeagueRepositoryCallback
import project.kotlin.submissionkade2.models.DetailLeagueResponse
import project.kotlin.submissionkade2.view.DetailLeagueView

class DetailLeaguePresenterTest {

    @Mock
    private lateinit var view: DetailLeagueView

    @Mock
    private lateinit var leagueRepository: LeagueRepository

    @Mock
    private lateinit var detailLeagueResponse: DetailLeagueResponse

    private lateinit var detailLeaguePresenter: DetailLeaguePresenter


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        detailLeaguePresenter = DetailLeaguePresenter(view, leagueRepository)
    }

    @Test
    fun getLeagueLoadedTest() {

        val id = 4328

        detailLeaguePresenter.getLeague(id)

        argumentCaptor<LeagueRepositoryCallback<DetailLeagueResponse?>>().apply {

            verify(leagueRepository).getDetailLeague(eq(id), capture())
            firstValue.onDataLeagueLoaded(detailLeagueResponse)
        }

        verify(view).onShowLoadingDetailLeague()
        verify(view).onDataLeagueLoaded(detailLeagueResponse)
        verify(view).onHideLoadingDetailLeague()
    }

    @Test
    fun getLeagueErrorTest() {

        detailLeaguePresenter.getLeague(0)

        argumentCaptor<LeagueRepositoryCallback<DetailLeagueResponse?>>().apply {

            verify(leagueRepository).getDetailLeague(eq(0), capture())
            firstValue.onDataLeagueError()
        }

        verify(view).onShowLoadingDetailLeague()
        verify(view).onDataLeagueError()
        verify(view).onHideLoadingDetailLeague()
    }
}