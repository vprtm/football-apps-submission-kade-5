package project.kotlin.submissionkade2.presenterTest

import com.nhaarman.mockito_kotlin.argumentCaptor
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import project.kotlin.submissionkade2.Presenter.EventPresenter
import project.kotlin.submissionkade2.Repository.EventRepository
import project.kotlin.submissionkade2.Repository.EventRepositoryCallback
import project.kotlin.submissionkade2.models.EventResponse
import project.kotlin.submissionkade2.view.EventView

class EventPresenterTest {

    @Mock
    private lateinit var view: EventView

    @Mock
    private lateinit var eventRepository: EventRepository

    @Mock
    private lateinit var eventResponse: EventResponse

    private lateinit var eventPresenter: EventPresenter


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        eventPresenter = EventPresenter(view, eventRepository)
    }

    @Test
    fun getNextTest() {

        val id = 4328

        eventPresenter.getNext(id)

        argumentCaptor<EventRepositoryCallback<EventResponse?>>().apply {

            verify(eventRepository).getNextMatch(eq(id), capture())
            firstValue.onDataLoadedEvent(eventResponse)
        }

        verify(view).onShowLoadingEvent()
        verify(view).onDataLoadedEvent(eventResponse)
        verify(view).onHideLoadingEvent()
    }

    @Test
    fun getPreviousTest() {

        val id = 4328

        eventPresenter.getPrevious(id)

        argumentCaptor<EventRepositoryCallback<EventResponse?>>().apply {

            verify(eventRepository).getPreviousMatch(eq(id), capture())
            firstValue.onDataLoadedEvent(eventResponse)
        }

        verify(view).onShowLoadingEvent()
        verify(view).onDataLoadedEvent(eventResponse)
        verify(view).onHideLoadingEvent()
    }
}