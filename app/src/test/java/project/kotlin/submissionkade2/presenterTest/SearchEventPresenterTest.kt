package project.kotlin.submissionkade2.presenterTest

import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.eq
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations.*
import project.kotlin.submissionkade2.Presenter.SearchEventPresenter
import project.kotlin.submissionkade2.Repository.EventRepository
import project.kotlin.submissionkade2.Repository.EventRepositoryCallback
import project.kotlin.submissionkade2.models.SearchResponse
import project.kotlin.submissionkade2.view.SearchEventView


class SearchEventPresenterTest {

    @Mock
    private lateinit var view: SearchEventView

    @Mock
    private lateinit var eventRepository: EventRepository

    @Mock
    private lateinit var searchResponse: SearchResponse

    private lateinit var searchEventPresenter: SearchEventPresenter



    @Before
    fun setUp() {
        initMocks(this)

        searchEventPresenter = SearchEventPresenter(view, eventRepository)

    }

    @Test
    fun getSearchLoadedTest() {

        val query = "Racing Club"

        searchEventPresenter.getSearch(query)

        argumentCaptor<EventRepositoryCallback<SearchResponse?>>().apply {

            verify(eventRepository).searchMatch(eq(query), capture())
            firstValue.onDataLoadedEvent(searchResponse)
        }

        verify(view).onShowLoadingSearch()
        verify(view).onDataLoadedEvent(searchResponse)
        verify(view).onHideLoadingSearch()
    }

    @Test
    fun getSearchErrorTest() {

        val query = "123"

        searchEventPresenter.getSearch(query)

        argumentCaptor<EventRepositoryCallback<SearchResponse?>>().apply {

            verify(eventRepository).searchMatch(eq(query), capture())
            firstValue.onDataErrorEvent()
        }

        verify(view).onShowLoadingSearch()
        verify(view).onDataErrorEvent()
        verify(view).onHideLoadingSearch()
    }
}
